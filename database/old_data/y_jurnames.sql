-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Сен 16 2017 г., 21:00
-- Версия сервера: 5.5.44
-- Версия PHP: 5.3.3-7+squeeze19

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `weltour`
--

-- --------------------------------------------------------

--
-- Структура таблицы `y_jurnames`
--

CREATE TABLE IF NOT EXISTS `y_jurnames` (
  `jur_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `jur_type` int(10) unsigned DEFAULT '0',
  `jur_name` varchar(255) DEFAULT NULL,
  `jur_adress` longtext,
  `jur_phone` varchar(255) DEFAULT NULL,
  `jur_gen` varchar(255) DEFAULT NULL,
  `jur_ogrn` varchar(255) DEFAULT NULL,
  `jur_logo` int(10) unsigned DEFAULT '0',
  `jur_isactive` enum('1','0') DEFAULT '0',
  `jur_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `jur_town` varchar(255) DEFAULT NULL,
  `jur_okpo` varchar(255) DEFAULT NULL,
  `jur_okato` varchar(255) DEFAULT NULL,
  `jur_office` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`jur_id`),
  KEY `jur_type` (`jur_type`),
  KEY `jur_logo` (`jur_logo`),
  KEY `jur_office` (`jur_office`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=661 ;

--
-- Дамп данных таблицы `y_jurnames`
--

INSERT INTO `y_jurnames` (`jur_id`, `jur_type`, `jur_name`, `jur_adress`, `jur_phone`, `jur_gen`, `jur_ogrn`, `jur_logo`, `jur_isactive`, `jur_timestamp`, `jur_town`, `jur_okpo`, `jur_okato`, `jur_office`) VALUES
(637, 2, 'Виноградов Борис Анатольевич', '117638, г. Москва, ул. Криворожская 23-2-1', '8(495)777 09 71', 'Виноградов Борис Анатольевич', '312774619200345', 0, '1', '2016-08-02 10:27:47', 'Москва', '0117777064', '45296575000', 167);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
