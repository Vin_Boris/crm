-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Сен 16 2017 г., 20:47
-- Версия сервера: 5.5.44
-- Версия PHP: 5.3.3-7+squeeze19

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `weltour`
--

-- --------------------------------------------------------

--
-- Структура таблицы `y_offices`
--

CREATE TABLE IF NOT EXISTS `y_offices` (
  `of_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `of_parent` int(10) unsigned DEFAULT '0',
  `of_group` int(10) unsigned DEFAULT '0',
  `of_name` varchar(255) DEFAULT NULL,
  `of_prefix` varchar(255) DEFAULT NULL,
  `of_director` int(10) unsigned DEFAULT '0',
  `of_curator` int(10) unsigned DEFAULT '0',
  `of_jurid` int(10) unsigned DEFAULT '0',
  `of_phone` varchar(255) DEFAULT NULL,
  `of_fax` varchar(255) DEFAULT NULL,
  `of_address` longtext,
  `of_email` varchar(255) DEFAULT NULL,
  `of_descr` longtext,
  `of_contract` varchar(255) DEFAULT NULL,
  `of_contractdate` varchar(255) DEFAULT NULL,
  `of_discount` varchar(255) DEFAULT NULL,
  `of_isactive` enum('1','0') DEFAULT '0',
  `of_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `of_ip` varchar(255) DEFAULT NULL,
  `of_smscount` varchar(255) DEFAULT NULL,
  `of_addtermpm2trs` enum('1','0') DEFAULT '0',
  `of_region` int(10) unsigned DEFAULT '0',
  `of_bonusamount` varchar(255) DEFAULT NULL,
  `of_www` varchar(255) DEFAULT NULL,
  `of_adviserment` varchar(255) DEFAULT NULL,
  `of_shortorder` enum('1','0') DEFAULT '0',
  `of_town` int(10) unsigned DEFAULT '0',
  `of_comments` longtext,
  `of_purse` int(10) unsigned DEFAULT '0',
  `of_moneybox` decimal(10,2) DEFAULT NULL,
  `of_moneyboxonoff` enum('1','0') DEFAULT '0',
  PRIMARY KEY (`of_id`),
  KEY `of_parent` (`of_parent`),
  KEY `of_group` (`of_group`),
  KEY `of_director` (`of_director`),
  KEY `of_curator` (`of_curator`),
  KEY `of_jurid` (`of_jurid`),
  KEY `of_region` (`of_region`),
  KEY `of_town` (`of_town`),
  KEY `of_purse` (`of_purse`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=885 ;

--
-- Дамп данных таблицы `y_offices`
--

INSERT INTO `y_offices` (`of_id`, `of_parent`, `of_group`, `of_name`, `of_prefix`, `of_director`, `of_curator`, `of_jurid`, `of_phone`, `of_fax`, `of_address`, `of_email`, `of_descr`, `of_contract`, `of_contractdate`, `of_discount`, `of_isactive`, `of_timestamp`, `of_ip`, `of_smscount`, `of_addtermpm2trs`, `of_region`, `of_bonusamount`, `of_www`, `of_adviserment`, `of_shortorder`, `of_town`, `of_comments`, `of_purse`, `of_moneybox`, `of_moneyboxonoff`) VALUES
(272, 167, 1, 'Виноградов Б. А. ИП (Белорусская)', '897', 793, 266, 637, '+7 (495) 777-0971', NULL, 'г. Москва, Ленинградский пр-т, д. 2', 'be@touronliner.ru', 'Подключен 14:34 06.08.2012', '№ 14/1011-ФР', '1447016400', '7', '1', '2017-02-01 13:43:24', '95.84.202.5', '0', '', 11, '35407.03', NULL, NULL, '1', 0, NULL, 11920, '9486.31', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
