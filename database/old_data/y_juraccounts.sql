-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Сен 16 2017 г., 21:00
-- Версия сервера: 5.5.44
-- Версия PHP: 5.3.3-7+squeeze19

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `weltour`
--

-- --------------------------------------------------------

--
-- Структура таблицы `y_juraccounts`
--

CREATE TABLE IF NOT EXISTS `y_juraccounts` (
  `jra_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `jra_jurid` int(10) unsigned DEFAULT '0',
  `jra_rs` varchar(255) DEFAULT NULL,
  `jra_inn` varchar(255) DEFAULT NULL,
  `jra_kpp` varchar(255) DEFAULT NULL,
  `jra_bik` varchar(255) DEFAULT NULL,
  `jra_bank` varchar(255) DEFAULT NULL,
  `jra_ks` varchar(255) DEFAULT NULL,
  `jra_dop` longtext,
  `jra_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `jra_isactive` enum('1','0') DEFAULT '0',
  `jra_contract` enum('1','0') DEFAULT '0',
  `jra_check` enum('1','0') DEFAULT '0',
  PRIMARY KEY (`jra_id`),
  KEY `jra_jurid` (`jra_jurid`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=761 ;

--
-- Дамп данных таблицы `y_juraccounts`
--

INSERT INTO `y_juraccounts` (`jra_id`, `jra_jurid`, `jra_rs`, `jra_inn`, `jra_kpp`, `jra_bik`, `jra_bank`, `jra_ks`, `jra_dop`, `jra_timestamp`, `jra_isactive`, `jra_contract`, `jra_check`) VALUES
(733, 637, '40802810138040008543', '772577496002', '775003035', '044525225', 'в Московский банк ОАО «Сбербанк России»', '30101810400000000225', NULL, '2016-08-02 10:26:12', '0', '1', '0');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
