-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Сен 16 2017 г., 20:44
-- Версия сервера: 5.5.44
-- Версия PHP: 5.3.3-7+squeeze19

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `weltour`
--

-- --------------------------------------------------------

--
-- Структура таблицы `y_hotelstars`
--

CREATE TABLE IF NOT EXISTS `y_hotelstars` (
  `hts_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `hts_searchid` int(10) unsigned DEFAULT '0',
  `hts_name` varchar(255) DEFAULT NULL,
  `hts_sort` varchar(255) DEFAULT NULL,
  `hts_isactive` enum('1','0') DEFAULT '0',
  `hts_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`hts_id`),
  KEY `hts_searchid` (`hts_searchid`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=16 ;

--
-- Дамп данных таблицы `y_hotelstars`
--

INSERT INTO `y_hotelstars` (`hts_id`, `hts_searchid`, `hts_name`, `hts_sort`, `hts_isactive`, `hts_timestamp`) VALUES
(1, 1, '1*', '0', '1', '2012-06-04 11:15:11'),
(2, 2, '2*', '0', '1', '2012-06-04 11:15:11'),
(3, 3, '3*', '0', '1', '2012-06-04 11:15:11'),
(4, 4, '4*', '0', '1', '2012-06-04 11:15:11'),
(5, 5, '5*', '0', '1', '2012-06-04 11:15:11'),
(6, 7, 'HV', '0', '1', '2012-06-04 11:15:11'),
(7, 10, 'Пансионат', '0', '1', '2012-06-04 11:15:11'),
(8, 14, 'База отдыха', '0', '1', '2012-06-04 11:15:11'),
(9, 15, 'Отель по программе', '0', '1', '2012-06-04 11:15:11'),
(10, 16, 'Апартаменты', '0', '1', '2012-06-04 11:15:11'),
(11, 13, 'Звездность неизвестна', '0', '1', '2012-06-04 11:15:11'),
(12, 18, 'Санаторий', '0', '1', '2012-06-04 11:15:11'),
(13, 19, 'Дом отдыха', '0', '1', '2012-06-04 11:15:11'),
(14, 20, 'Детский оздоровительный лагерь', '0', '1', '2012-06-04 11:15:11'),
(15, 21, 'Курортный комплекс', '0', '1', '2012-06-04 11:15:11');

-- --------------------------------------------------------

--
-- Структура таблицы `y_jurtypes`
--

CREATE TABLE IF NOT EXISTS `y_jurtypes` (
  `jrt_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `jrt_name` varchar(255) DEFAULT NULL,
  `jrt_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`jrt_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `y_jurtypes`
--

INSERT INTO `y_jurtypes` (`jrt_id`, `jrt_name`, `jrt_timestamp`) VALUES
(1, 'ООО', '2012-02-28 10:53:21'),
(2, 'ИП', '2012-02-28 10:53:21'),
(3, 'ЗАО', '2012-02-28 10:53:21'),
(4, 'ОАО', '2015-03-10 18:33:47'),
(5, 'НОУ', '2015-03-10 18:33:47');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
