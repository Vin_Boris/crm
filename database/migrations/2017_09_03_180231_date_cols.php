<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DateCols extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::table('clients', function(Blueprint $table)
        {
            $table->dropColumn('int_passport_till');
            $table->dropColumn('visa_till');
            $table->dropColumn('birthday');
            $table->dropColumn('have_visa');
        });

        Schema::table('clients', function(Blueprint $table)
        {

            $table->date( 'birthday')->nullable();
            $table->date( 'int_passport_till')->nullable();
            $table->date( 'visa_till')->nullable();
            $table->boolean( 'have_visa')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
