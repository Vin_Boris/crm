<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransfersFix extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::table('transfers', function(Blueprint $table)
        {

            $table->integer('transfer');
            $table->integer('order_id');
            $table->integer('town_id');
            $table->dropColumn('cost');
            $table->timestamps();
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::table('transfers', function(Blueprint $table)
        {
            $table->integer('cost');
            $table->dropColumn('transfer');
            $table->dropColumn('order_id');
            $table->dropColumn('town_id');
            $table->dropTimestamps();
        });
	}

}
