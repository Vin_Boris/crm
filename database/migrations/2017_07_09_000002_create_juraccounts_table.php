<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJuraccountsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('legal_entity_accounts', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('jurname_id')->unsigned();;
            $table->string('rs');
            $table->string('bik');
            $table->string('bank');
            $table->string('ks');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('legal_entity_accounts');
    }

}
