<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTouropsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tourops', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('efrt_number');
            $table->string('short_name');
            $table->string('full_name');
            $table->string('address');
            $table->string('www');
            $table->string('inn');
            $table->string('ogrn');
            $table->integer('is_member');
            $table->string('units_address');
            $table->string('full_product_price');
            $table->integer('prev_year_tourists_count');
            $table->integer('reserve_amount');
            $table->integer('responsibility_amount');
            $table->integer('year_contribution_amount');
            $table->integer('finsupply_amount_full');
            $table->string('finsupply_docs');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tourops');
    }

}
