<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccomodationOrdersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accomodation_orders', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('holel_id')->unsigned();;
            $table->integer('accomodation_id')->unsigned();;
            $table->integer('food_id')->unsigned();;
            $table->integer('cost');
            $table->date('date_from');
            $table->date('date_to');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('accomodation_orders');
    }

}
