<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClientCols extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::table('clients', function(Blueprint $table)
        {

            $table->text('first_name_eng');
            $table->text('last_name_eng');

            $table->text('passport_gift');
            $table->text('international_passport_gift');
            $table->integer('have_visa');
            $table->date('visa_till');
            $table->text('type');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//

        Schema::table('clients', function(Blueprint $table)
        {
            $table->dropColumn('first_name_eng');
            $table->dropColumn('last_name_eng');


            $table->dropColumn('passport_gift');
            $table->dropColumn('international_passport_gift');
            $table->dropColumn('have_visa');
            $table->dropColumn('visa_till');
            $table->dropColumn('type');
        });
	}

}
