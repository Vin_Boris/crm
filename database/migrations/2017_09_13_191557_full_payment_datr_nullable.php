<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FullPaymentDatrNullable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::table('orders', function(Blueprint $table)
        {
            $table->dropColumn('full_payment_date');
        });

        Schema::table('orders', function(Blueprint $table)
        {
            $table->date('full_payment_date')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::table('orders', function(Blueprint $table)
        {
            $table->dropColumn('full_payment_date');
        });

        Schema::table('orders', function(Blueprint $table)
        {
            $table->date('full_payment_date');

        });
	}

}
