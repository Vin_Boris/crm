<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAccomodationColums extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accomodation_orders', function(Blueprint $table)
        {

            $table->integer('country_id');
            $table->integer('town_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('accomodation_orders', function(Blueprint $table)
        {

            $table->dropColumn('country_id');
            $table->dropColumn('town_id');

        });
    }

}
