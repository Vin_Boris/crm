<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTicketDates extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::table('tickets', function(Blueprint $table)
        {
            $table->date('date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @returndatabase/migrations/2017_08_30_123705_add_tickets_cols.php void
     */
    public function down()
    {
        //
        Schema::table('tickets', function(Blueprint $table)
        {
            $table->dropColumn('date');

        });
    }

}
