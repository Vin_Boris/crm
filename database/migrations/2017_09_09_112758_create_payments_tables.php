<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::create('client_payments', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->string('sign');
            $table->integer('amount');
            $table->tinyInteger('type');
            $table->date('date')->nullable();
            $table->timestamps();
        });

        Schema::create('tourop_payments', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->string('sign');
            $table->integer('amount');
            $table->integer('amount_ue');
            $table->integer('exchange_rate');
            $table->date('date')->nullable();
            $table->tinyInteger('type');
            $table->timestamps();
        });

        Schema::create('order_confirmations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->integer('amount_ue');
            $table->integer('currency');
            $table->date('date')->nullable();
            $table->string('number');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//

        Schema::drop('client_payments');
        Schema::drop('tourop_payments');
        Schema::drop('order_confirmations');
	}

}
