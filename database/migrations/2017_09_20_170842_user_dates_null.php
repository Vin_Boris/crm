<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserDatesNull extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::table('legal_entities', function(Blueprint $table)
        {
            $table->dropColumn('address');
            $table->dropColumn('phone');
            $table->dropColumn('director_fio');
            $table->dropColumn('inn');
            $table->dropColumn('kpp');
            $table->dropColumn('ogrn');
            $table->dropColumn('town');
            $table->dropColumn('okpo');
            $table->dropColumn('okato');

        });

        Schema::table('legal_entities', function(Blueprint $table)
        {
            $table->string('address')->nullable();
            $table->string('phone')->nullable();
            $table->string('director_fio')->nullable();
            $table->string('inn')->nullable();
            $table->string('kpp')->nullable();
            $table->string('ogrn')->nullable();
            $table->string('town')->nullable();
            $table->string('okpo')->nullable();
            $table->string('okato')->nullable();
        });

        Schema::table('users', function(Blueprint $table)
        {
            $table->dropColumn('birthday');

        });
        Schema::table('users', function(Blueprint $table)
        {
            $table->date('birthday')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @returndatabase/migrations/2017_08_30_123705_add_tickets_cols.php void
     */
    public function down()
    {
        //

    }

}
