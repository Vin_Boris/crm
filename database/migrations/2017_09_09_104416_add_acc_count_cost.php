<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAccCountCost extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::table('accomodation_orders', function(Blueprint $table)
        {
            $table->integer('cost');
            $table->integer('count');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::table('accomodation_orders', function(Blueprint $table)
        {
            $table->dropColumn('cost');
            $table->dropColumn('count');
        });
	}

}
