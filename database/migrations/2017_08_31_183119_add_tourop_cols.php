<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTouropCols extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::table('tourops', function(Blueprint $table)
        {
            $table->dropColumn('finsupply_docs');
            $table->longText('finsupply_docs_text');
            $table->text('sphere_of_activity');
            $table->text('order_number');
            $table->text('order_date');
            $table->text('certificate_number');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::table('tourops', function(Blueprint $table)
        {
            $table->text('finsupply_docs');
            $table->dropColumn('finsupply_docs_text');
            $table->dropColumn('sphere_of_activity');
            $table->dropColumn('order_number');
            $table->dropColumn('order_date');
            $table->dropColumn('certificate_number');
        });
	}

}
