<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTicketsCols extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//

        Schema::table('tickets', function(Blueprint $table)
        {

            $table->integer('town_from');
            $table->integer('town_to');
            $table->text('description');

        });

        Schema::table('accomodation_orders', function(Blueprint $table)
        {

            $table->renameColumn('holel_id','hotel_id');
            $table->timestamps();
            $table->dropColumn('cost');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @returndatabase/migrations/2017_08_30_123705_add_tickets_cols.php void
	 */
	public function down()
	{
		//
        Schema::table('tickets', function(Blueprint $table)
        {
            $table->dropColumn('description');
            $table->dropColumn('town_from');
            $table->dropColumn('town_to');

        });

        Schema::table('accomodation_orders', function(Blueprint $table)
        {

            $table->renameColumn('hotel_id', 'holel_id');
            $table->dropTimestamps();
            $table->integer('cost');
        });
	}

}
