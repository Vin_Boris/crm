<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJurnamesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('legal_entities', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('orgform_id')->unsigned();
            $table->string('name');
            $table->string('address');
            $table->string('phone');
            $table->string('director_fio');
            $table->string('inn');
            $table->string('kpp');
            $table->string('ogrn');
            $table->string('town');
            $table->string('okpo');
            $table->string('okato');

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('legal_entities');
    }

}
