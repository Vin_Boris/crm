<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TouropFieldsNull extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::table('tourops', function(Blueprint $table)
        {
            $table->dropColumn('address');
            $table->dropColumn('www');
            $table->dropColumn('inn');
            $table->dropColumn('ogrn');
            $table->dropColumn('is_member');
            $table->dropColumn('units_address');
            $table->dropColumn('full_product_price');
            $table->dropColumn('prev_year_tourists_count');
            $table->dropColumn('reserve_amount');
            $table->dropColumn('responsibility_amount');
            $table->dropColumn('year_contribution_amount');
            $table->dropColumn('finsupply_amount_full');
            $table->dropColumn('finsupply_docs_text');
            $table->dropColumn('sphere_of_activity');
            $table->dropColumn('order_number');
            $table->dropColumn('order_date');
            $table->dropColumn('certificate_number');

        });

        Schema::table('tourops', function(Blueprint $table)
        {

            $table->string('address')->nullable();
            $table->string('www')->nullable();;
            $table->string('inn')->nullable();;
            $table->string('ogrn')->nullable();;
            $table->integer('is_member')->nullable();;
            $table->string('units_address')->nullable();;
            $table->string('full_product_price')->nullable();;
            $table->integer('prev_year_tourists_count')->nullable();;
            $table->integer('reserve_amount')->nullable();;
            $table->integer('responsibility_amount')->nullable();;
            $table->integer('year_contribution_amount')->nullable();;
            $table->integer('finsupply_amount_full')->nullable();;
            $table->longText('finsupply_docs_text')->nullable();;
            $table->text('sphere_of_activity')->nullable();;
            $table->text('order_number')->nullable();;
            $table->text('order_date')->nullable();;
            $table->text('certificate_number')->nullable();;
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
