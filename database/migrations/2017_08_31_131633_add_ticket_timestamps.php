<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTicketTimestamps extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//

        Schema::table('tickets', function(Blueprint $table)
        {

            $table->timestamps();
            $table->dropColumn('description');
        });

        Schema::table('insurance', function(Blueprint $table)
        {

            $table->timestamps();
        });

        Schema::table('visas', function(Blueprint $table)
        {

            $table->timestamps();
        });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//

        Schema::table('tickets', function(Blueprint $table)
        {

            $table->dropTimestamps();
            $table->text('description');
        });

        Schema::table('insurance', function(Blueprint $table)
        {

            $table->dropTimestamps();
        });

        Schema::table('visas', function(Blueprint $table)
        {

            $table->dropTimestamps();
        });
	}

}
