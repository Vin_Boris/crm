<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

        $org_forms_ooo = DB::table('legal_forms')->insert([
            'name' => 'ООО',
            'position' => 0 ]);


        $jurname = DB::table('legal_entities')->insert([
            'orgform_id'   => $org_forms_ooo ,
            'name'         => 'Туризм' ,
            'address'      => '' ,
            'phone'        => '' ,
            'director_fio' => '' ,
            'inn'          => '' ,
            'kpp'          => '' ,
            'ogrn'         => '' ,
            'town'         => '' ,
            'okpo'         => '' ,
            'okato'        => '' ]) ;

        $office =DB::table('offices')->insert([
            'jurname_id' => $jurname ,
            'name'       => 'Офис' ,
            'phone'      => '' ,
            'address'    => '' ,
            'email'      => '' ]);

        DB::table('users')->insert([
            'office_id'   => $office ,
            'first_name'  => '' ,
            'middle_name' => 'Администратор' ,
            'last_name'   => '' ,
            'password'    => bcrypt(env('ADMIN_PASSWORD')) ,
            'role'        => '' ,
            'email'       =>  env('ADMIN_EMAIL') ,
            'phone'       => '' ,
            'birthday'    => '2017-01-01' ,
            'sex'         => 1 ,
            'isactive'    => 1]);

	}

}
