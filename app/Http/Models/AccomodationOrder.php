<?php
namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class AccomodationOrder extends Model
{

    protected $fillable = [
        'order_id',
        'country_id',
        'town_id',
        'hotel_id',
        'accomodation_id',
        'food_id',
        'date_from',
        'date_to',
        'cost',
        'count'
    ];

    public function country(){
        return $this->belongsTo( Country::class, 'country_id' );
    }

    public function town(){
        return $this->belongsTo( Town::class, 'town_id' );
    }

    public function hotel(){
        return $this->belongsTo( Hotel::class, 'hotel_id' );
    }

    public function accomodation(){
        return $this->belongsTo( Accomodation::class, 'accomodation_id' );
    }

    public function food(){
        return $this->belongsTo( Food::class, 'food_id' );
    }

}