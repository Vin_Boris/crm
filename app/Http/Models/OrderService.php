<?php

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class OrderService extends Model
{
    protected $fillable = [
        'order_id',
        'name',
        'cost',
        'count'
    ];
}