<?php namespace App\Http\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends BaseModel implements AuthenticatableContract, CanResetPasswordContract {

    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function fullname(){
        return $this->last_name . " " . $this->first_name . " " . $this->middle_name;
    }

    public function office(){
        return $this->belongsTo( Office::class, 'office_id');
    }

    public function short_name(){
        return ucfirst( $this->last_name ) . " " . substr( $this->first_name,0,1) . " " . substr( $this->middle_name,0,1);
    }

    public function getBirthdayAttribute(){
        return parent::parseDate('birthday' );
    }

    public function setBirthdayAttribute( $value ){
        parent::setDate('birthday', $value );
    }

    public function setPasswordAttribute( $value ){
        if( $value != '******' ){
            $this->attributes['password'] = bcrypt($value);
        }
    }
}