<?php

namespace App\Http\Models;


class Currency
{
    const RUB = 1;
    const USD = 2;
    const EUR = 3;

    protected static $curr_names = [
        self::RUB => 'RUB',
        self::USD => 'USD',
        self::EUR => 'EUR'
    ];

}