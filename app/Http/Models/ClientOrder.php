<?php

namespace App\Http\Models;


class ClientOrder extends BaseModel
{
    protected $fillable = [
        'order_id',
        'client_id',
        'date_from',
        'date_to'
    ];

    public function client(){
        return $this->belongsTo( Client::class, 'client_id');
    }

    public function order(){
        return $this->belongsTo( Order::class, 'order_id');
    }

    public function getDateFromAttribute(){
        return parent::parseDate('date_from' );
    }

    public function setDateFromAttribute( $value ){
        parent::setDate('date_from', $value );
    }

    public function getDateToAttribute(){
        return parent::parseDate('date_to' );
    }

    public function setDateToAttribute( $value ){
        parent::setDate('date_to', $value );
    }
}