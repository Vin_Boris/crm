<?php

namespace App\Http\Models;


class Ticket extends BaseModel
{

    const AVIA_ECONOM = 0;
    const RAILWAY = 1;

    public static $types = [
        self::AVIA_ECONOM => 'Авиаэконом',
        self::RAILWAY   => 'ЖД'
    ];

    protected $fillable = [
        'order_id',
        'type',
        'cost',
        'count',
        'town_from',
        'town_to',
        'date'
    ];

    public function from_town(){
        return $this->belongsTo( Town::class, 'town_from');
    }

    public function to_town(){
        return $this->belongsTo( Town::class, 'town_to');
    }

    public function getTypeNameAttribute(){
        return self::$types[ $this->type ];
    }

    public function getDateAttribute(){
        return parent::parseDate('date' );
    }

    public function setDateAttribute( $value ){
        parent::setDate('date', $value );
    }
}