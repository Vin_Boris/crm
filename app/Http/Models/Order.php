<?php

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Order  extends BaseModel
{

    public function user(){
        return $this->belongsTo( User::class, 'user_id' );
    }

    public function client_for_contract(){
        return $this->belongsTo( Client::class, 'client_id' );
    }

    public function tourop(){
        return $this->belongsTo( Tourop::class, 'tourop_id' );
    }

    public function clients(){
        return $this->hasMany( ClientOrder::class, 'order_id' );
    }

    public function accomodations(){
        return $this->hasMany( AccomodationOrder::class, 'order_id' );
    }

    public function tickets(){
        return $this->hasMany( Ticket::class, 'order_id' );
    }

    public function transfers(){
        return $this->hasMany( Transfer::class, 'order_id' );
    }

    public function insurance(){
        return $this->hasMany( Insurance::class, 'order_id' );
    }

    public function visas(){
        return $this->hasMany( Visa::class, 'order_id' );
    }

    public function services(){
        return $this->hasMany( OrderService::class, 'order_id' );
    }

    public function client_payments(){
        return $this->hasMany( ClientPayment::class, 'order_id' );
    }

    public function tourop_payments(){
        return $this->hasMany( TouropPayment::class, 'order_id' );
    }

    public function confirmations(){
        return $this->hasMany( OrderConfirmation::class, 'order_id' );
    }

    public function full_price(){
        return $this->cost - $this->cost/100*$this->discount;
    }

    public function getVisasPriceAttribute(){

        $sum = 0;
        foreach ( $this->visas as $visa ){
            $sum += $visa->cost;
        }

        return $sum;
    }

    public function getServicesPriceAttribute(){

        $sum = 0;
        foreach ( $this->services as $visa ){
            $sum += $visa->cost;
        }

        return $sum;
    }

    public function getDiscountSumAttribute(){
        return ceil( $this->cost /100 * $this->discount );
    }


    public function getFullPaymentDateAttribute(){
        return parent::parseDate('full_payment_date' );
    }

    public function setFullPaymentDateAttribute( $value ){
        parent::setDate('full_payment_date', $value );
    }
}