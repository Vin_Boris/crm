<?php

namespace App\Http\Models;


class ClientPayment extends BaseModel
{
    const TYPE_CASH = 0;
    const TYPE_NON_CASH = 1;

    protected $fillable = [
        'order_id',
        'sign',
        'amount',
        'type',
        'date'
    ];

    public function order(){
        return $this->belongsTo( Order::class, 'order_id');
    }

    public function getDateAttribute(){
        return parent::parseDate('date' );
    }

    public function setDateAttribute( $value ){
        parent::setDate('date', $value );
    }

}