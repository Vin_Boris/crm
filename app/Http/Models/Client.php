<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;


class Client extends BaseModel {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['first_name', 'middle_name', 'last_name'];

    public function user(){
        return $this->hasOne( User::class, 'id' , 'user_id' );
    }

    public function orders(){
        return $this->hasMany( ClientOrder::class, 'client_id' );
    }

    public function fullname(){
        return $this->last_name . " " . $this->first_name . " " . $this->middle_name;
    }


    public function short_name(){
        return ucfirst( $this->last_name ) . " " . substr( $this->first_name,0,1) . " " . substr( $this->middle_name,0,1);
    }

    public function fullname_eng(){
        return   $this->first_name_eng . " " . $this->last_name_eng;
    }

    public function getIntPassportTillAttribute(){
        return parent::parseDate('int_passport_till' );
    }

    public function setIntPassportTillAttribute( $value ){
        parent::setDate('int_passport_till', $value );
    }

    public function getBirthdayAttribute(){
       return parent::parseDate('birthday' );
    }

    public function setBirthdayAttribute( $value ){
        parent::setDate('birthday', $value );
    }

    public function getVisaTillAttribute(){
        return parent::parseDate('visa_till' );
    }

    public function setVisaTillAttribute( $value ){
        parent::setDate('visa_till', $value );
    }

}