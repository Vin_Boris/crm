<?php

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{
    const AIRPORT_HOTEL = 0;
    const HOTEL_AIRPORT = 1;

    const TYPE_GROUP = 0;
    const TYPE_INDIVIDUAL = 1;

    public static $directions = [
        self::AIRPORT_HOTEL => 'Аэропорт отель',
        self::HOTEL_AIRPORT => 'Отель аэропорт'
    ];

    public static $types = [
        self::TYPE_GROUP => 'Групповой',
        self::TYPE_INDIVIDUAL => 'Индивидуальный'
    ];

    protected $fillable = [
        'order_id',
        'type',
        'town_id',
        'transfer',
        'count'
    ];

    public function town(){
        return $this->belongsTo( Town::class, 'town_id');
    }

    public function getDirectionAttribute(){
        return self::$directions[ $this->transfer ];
    }

    public function getTypeNameAttribute(){
        return self::$types[ $this->type ];
    }
}