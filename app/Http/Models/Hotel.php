<?php

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    protected $fillable = ['name'];

    public function getNameAttribute(){

        if( $this->attributes['stars'] > 0 ){
            return $this->attributes['name'] . ' ' . $this->attributes['stars'] . "*";
        }else{
            return $this->attributes['name'];
        }
    }

    public function setNameAttribute( $name ){

        preg_match( '/([^\*]+)(([0-9])\*)/i', $name, $matches );

        if( isset( $matches[3] ) ){
            $this->attributes['name'] = $matches[1];
            $this->attributes['stars'] = $matches[3];
        }else{
            $this->attributes['name'] = $name;
            $this->attributes['stars'] = 0;
        }
    }
}