<?php

namespace App\Http\Models;

use Carbon\Carbon;

class BaseModel extends \Illuminate\Database\Eloquent\Model {

    /**
     * Return a timestamp as DateTime object.
     *
     * Extended to allow saving empty date values as NULL in the database.
     *
     * @param  mixed $attr
     * @param string $format
     * @return string
     */
    protected function parseDate( $attr, $format = 'd.m.Y' ) {

        if( isset( $this->attributes[ $attr ] )  && strlen( $this->attributes[ $attr ] ) >0 ){
            return Carbon::parse(  $this->attributes[ $attr ] )->format( $format );
        }else{
            return null;
        }
    }

    protected function setDate( $attr, $value ){
        if( strlen( $value ) >0 ){
            $this->attributes[ $attr ] = Carbon::parse( $value );
        }else{
            $this->attributes[ $attr ] = null;
        }
    }

}