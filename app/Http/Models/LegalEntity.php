<?php namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class LegalEntity extends  Model
{

    public function legal_form(){
        return $this->belongsTo( LegalForm::class, 'orgform_id' );
    }

    public function accounts(){
        return $this->hasMany( LegalEntityAccount::class, 'jurname_id' );
    }

    public function getAccountAttribute(){
        return $this->accounts()->first();
    }

    public function fullname(){
        return $this->legal_form->name . " " . $this->name;
    }
}