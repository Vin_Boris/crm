<?php

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Food extends Model
{
    protected $fillable = ['name'];
}