<?php

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Town extends Model
{
    protected $fillable = [ 'name', 'country_id' ];
}