<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Office extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];


    public function legal_entity(){
        return $this->hasOne( LegalEntity::class, 'id' , 'jurname_id' );
    }
}