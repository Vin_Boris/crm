<?php
namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Visa extends Model
{

    protected $fillable = [
        'order_id',
        'country_id',
        'cost',
        'count'
    ];

    public function country(){
        return $this->belongsTo( Country::class, 'country_id' );
    }
}