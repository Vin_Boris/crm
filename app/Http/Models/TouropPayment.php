<?php

namespace App\Http\Models;


class TouropPayment extends BaseModel
{

    const TYPE_CASH = 0;
    const TYPE_NON_CASH = 1;

    const RUB = 0;
    CONST USD = 1;
    const EUR = 2;

    protected $fillable = [
        'order_id',
        'sign',
        'amount',
        'amount_ue',
        'exchange_rate',
        'type',
        'date'
    ];

    public function order(){
        return $this->belongsTo( Order::class, 'order_id');
    }

    public function getDateAttribute(){
        return parent::parseDate('date' );
    }

    public function setDateAttribute( $value ){
        parent::setDate('date', $value );
    }
}