<?php

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Accomodation extends Model
{
    protected $table = 'accomodation';

    protected $fillable = ['name'];
}