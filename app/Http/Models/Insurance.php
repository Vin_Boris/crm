<?php

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Insurance extends Model
{
    const MEDICAL = 1;
    const UNABLE_TRAVEL = 2;

    protected $table = 'insurance';

    public static $types = [
        self::MEDICAL => 'Медицинская',
        self::UNABLE_TRAVEL => 'От невыезда'
    ];

    protected $fillable = [
        'order_id',
        'type',
        'cost',
        'count'
    ];

    public function getTypeNameAttribute(){
        return self::$types[ $this->type ];
    }
}