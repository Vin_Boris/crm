<?php namespace App\Http\Controllers;


use App\Http\Models\Tourop;
use Illuminate\Http\Request;

class TouropController extends Controller {

    public function index(){
        $tourops = Tourop::get();

        return view( 'tourops/list', [ 'data' => $tourops ] );
    }

    public function edit( $id = 0){

        $tourop = Tourop::find( $id );

        if( !$tourop ){
            $tourop = new Tourop();
            if( !empty( \Input::old() ) ) {
                self::fillTouropFromInput($tourop, \Input::old() );
            }
        }

        return view( 'tourops/form', [ 'tourop' => $tourop ] );
    }

    public function save( Request $request , $id = 0 ){

        $this->validate($request, [
            'efrt_number'              => 'required|max:255',
            'short_name'               => 'required|max:255',
            'full_name'                => 'required|max:255'
        ],[
            'efrt_number.required'              => 'Номер ЕФРТ обязателен для заполнения',
            'short_name.required'               => 'Короткое название обязательно для заполнения',
            'full_name.required'                => 'Название обязательно для заполнения'
        ]);

        $post = \Input::all();

        $tourop = Tourop::find( $id );

        if( !$tourop ) {
            $tourop = new Tourop();
        }

        self::fillTouropFromInput( $tourop, $post );

        $tourop->save();

        return response()->redirectTo( "/tour_operators" );
    }

    private static function fillTouropFromInput( &$tourop, $post ){

        $tourop->efrt_number = $post['efrt_number'];
        $tourop->short_name = $post['short_name'];
        $tourop->full_name = $post['full_name'];
        $tourop->address = $post['address'];
        $tourop->www = $post['www'];
        $tourop->inn = $post['inn'];
        $tourop->ogrn = $post['ogrn'];
        $tourop->is_member = isset( $post['is_member'] ) ? $post['is_member'] : 0 ;
        $tourop->units_address = $post['unit_address'];
        $tourop->full_product_price = $post['full_product_price'];
        $tourop->prev_year_tourists_count = (int) $post['prev_year_tourists_count'];
        $tourop->reserve_amount = (int)  $post['reserve_amount'];
        $tourop->responsibility_amount = (int)  $post['responsibility_amount'];
        $tourop->year_contribution_amount =  (int)  $post['year_contribution_amount'];
        $tourop->finsupply_amount_full =  (int) $post['finsupply_amount_full'];
        $tourop->finsupply_docs_text = $post['finsupply_docs_text'];
    }

}
