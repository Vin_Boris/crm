<?php namespace App\Http\Controllers;


use App\Http\Models\Accomodation;
use App\Http\Models\Country;
use App\Http\Models\Food;
use App\Http\Models\Hotel;
use App\Http\Models\Town;
use Illuminate\Database\Eloquent\Builder;

class DictionaryController extends Controller {

    public function countries(){
        return response()->json( self::data( Country::query() ) );
    }

    public function countriesEdit(  $id = 0 ){
        return response()->json( self::edit( new Country(), $id ) );
    }

    public function towns( $country_id = 0 ){

        $query = Town::query();

        if( $country_id > 0){
            $query = Town::where( 'country_id', $country_id );
        }

        return response()->json( self::data( $query ) );
    }

    public function townsEdit( $country_id, $id = 0 ){
        return response()->json( self::edit( new Town(), $id, [ 'country_id' => $country_id ] ) );
    }

    public function hotels( $town_id ){

        return response()->json( self::data( Hotel::where( 'town_id' , $town_id ) ) );
    }

    public function hotelsEdit( $town_id, $id = 0 ){
        return response()->json( self::edit( new Hotel(), $id, [ 'town_id' => $town_id] ) );
    }

    public function food(){
        return response()->json( self::data( Food::query() ) );
    }

    public function foodEdit( $id = 0 ){
        return response()->json( self::edit( new Food(), $id ) );
    }

    public function accomodations(){
        return response()->json( self::data( Accomodation::query() ) );
    }

    public function accomodationsEdit( $id = 0 ){
        return response()->json( self::edit( new Accomodation(), $id ) );
    }

    private static function data( Builder $builder ){
        $term = \Input::get('term');
        return $builder->where('name', 'LIKE', $term .'%' )->orderBy('name')->get();
    }

    private static function edit( $model, $id, $attributes = [] ){

        $name = \Input::get('name');

        if( strlen( $name ) == 0 ){
            return $model;
        }

        $class = $model->find( $id );

        if( !$class ){
            $class = $model;
        }

        if( !isset( $attributes[ 'name' ] )) {
            $attributes['name'] = $name;
        }

        foreach ( $attributes as $attribute => $value ){
            $class->$attribute = $value;
        }

        $class->save();
        return $class;
    }

}
