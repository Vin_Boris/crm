<?php namespace App\Http\Controllers;


use App\Http\Models\Accomodation;
use App\Http\Models\AccomodationOrder;
use App\Http\Models\Client;
use App\Http\Models\ClientOrder;
use App\Http\Models\ClientPayment;
use App\Http\Models\Country;
use App\Http\Models\Hotel;
use App\Http\Models\Insurance;
use App\Http\Models\Order;
use App\Http\Models\OrderConfirmation;
use App\Http\Models\OrderService;
use App\Http\Models\Ticket;
use App\Http\Models\Tourop;
use App\Http\Models\TouropPayment;
use App\Http\Models\Town;
use App\Http\Models\Transfer;
use App\Http\Models\User;
use App\Http\Models\Visa;
use Carbon\Carbon;

class OrderController extends Controller {

    public function Index(){

        $filter = [
            'order_id'          => \Input::get('order_id', ''),
            'order_date_from'   => \Input::get('order_date_from', null ),
            'order_date_to'     => \Input::get('order_date_to', null ),
            'tour_date_from'    => \Input::get('tour_date_from', null ),
            'tour_date_to'      => \Input::get('tour_date_to', null ),
            'tourop_id'         => \Input::get('tourop_id', 0),
            'country_id'        => \Input::get('country_id', 0),
            'country_name'      => '',
            'user_id'           => \Input::get('user_id', 0),
        ];

        $orders = Order::with( [ 'accomodations.country', 'tourop', 'user' ] );

        if( (int) $filter[ 'order_id' ] >0 ){
            $orders->where( 'id', $filter[ 'order_id' ] );
        }

        if(  $filter[ 'order_date_from' ] &&  Carbon::createFromFormat( 'd.m.Y', $filter[ 'order_date_from' ] )->timestamp > 0 ){
            $orders->whereDate( 'created_at', '>=', Carbon::createFromFormat( 'd.m.Y', $filter[ 'order_date_from' ] )->startOfDay()->toDateTimeString() );
        }

        if(  $filter[ 'order_date_to' ] &&  Carbon::createFromFormat( 'd.m.Y', $filter[ 'order_date_to' ] )->timestamp > 0 ){
            $orders->whereDate( 'created_at', '<=', Carbon::createFromFormat( 'd.m.Y', $filter[ 'order_date_to' ] )->endOfDay()->toDateTimeString() );
        }


        if(  $filter[ 'tour_date_from' ] && Carbon::createFromFormat( 'd.m.Y', $filter[ 'tour_date_from' ] )->timestamp > 0 ){
            $orders->whereHas('clients', function( $q ) use( $filter ){
                $q->whereDate( 'date_from', '>=', Carbon::createFromFormat( 'd.m.Y', $filter[ 'tour_date_from' ] )->startOfDay()->toDateTimeString() );
            });
        }

        if(  $filter[ 'tour_date_to' ] && Carbon::createFromFormat( 'd.m.Y', $filter[ 'tour_date_to' ] )->timestamp > 0 ){
            $orders->whereHas('clients', function( $q ) use( $filter ){
                $q->whereDate( 'date_to', '>=', Carbon::createFromFormat( 'd.m.Y', $filter[ 'tour_date_to' ] )->endOfDay()->toDateTimeString() );
            });
        }

        if( (int) $filter[ 'country_id' ] >0 ){

            $country =  Country::find( $filter[ 'country_id' ] );

            if( $country ){
                $orders->whereHas('accomodations', function( $q ) use( $filter ){
                    $q->where( 'country_id', $filter[ 'country_id' ] );
                });

                $filter['country_name'] =$country->name;
            }


        }

        if( (int) $filter[ 'tourop_id' ] >0 ){
            $orders->where( 'tourop_id', $filter[ 'tourop_id' ] );
        }

        if( (int) $filter[ 'user_id' ] >0 ){
            $orders->where( 'user_id', $filter[ 'user_id' ] );
        }


        return view( 'orders/list', [
            'orders' =>  $orders->get(),
            'tourops' => Tourop::get(),
            'filter'  => $filter,
            'users'   => User::get()
        ] );

    }

    public function Edit( $id = 0 ){

        $order = Order::find( $id );

        if( !$order ){
            $order = new Order();
        }

        return view( 'orders/form', [
            'order' => $order,
            'tourops' => Tourop::get(),
            'tourists' => $order->clients()->with('client')->get(),
            'accomodations' => $order->accomodations()->get(),
            'transfers' => $order->transfers()->get(),
            'tickets' => $order->tickets()->get(),
            'insurances' => $order->insurance()->get(),
            'visas' => $order->visas()->get(),
            'services' => $order->services()->get(),
            'client_payments' => $order->client_payments()->get(),
            'tourop_payments' => $order->tourop_payments()->get(),
            'confirmations' => $order->confirmations()->get()
        ] );

    }

    public function Save( $id = 0 ){

        $order = Order::find( $id );

        if( !$order ){
            $order = new Order();
            $order->user_id = \Auth::user()->id;
        }

        $input = \Input::all();

        if( !isset( $input['tourop_id'] ) || $input['tourop_id'] == 0
        ){
            return response()->json( [
                'error' => 'Неверно заполнен туроператор'
            ] );
        }

        $tourop = Tourop::find( $input['tourop_id'] );

        if( !$tourop ){
            return response()->json( [
                'error' => 'Неверно заполнен туроператор'
            ] );
        }

        if( !isset( $input['clients'] )
        ){
            return response()->json( [
                'error' => 'Не заполнены клиенты'
            ] );
        }

        $order->tourop_id   = $tourop->id;
        $order->description = $input['description'];
        $order->cost        = (int) $input['price'] ;
        $order->discount    = (int) $input['discount'];
        $order->client_id    = $input['tourist_for_contract'];
        $order->full_payment_date =  $input['full_payment_date'];

        $order->save();

        $saved_clients = [];

        foreach ( $input['clients'] as $id => $client ){
            $cl = Client::find( $id );
            if( !$cl ){
                continue;
            }

            $client_order = ClientOrder::firstOrNew( [
                'client_id' => $cl->id,
                'order_id'  => $order->id
            ] );

            $client_order->date_from = $client['date_from'] ;
            $client_order->date_to = $client['date_to'] ;

            $client_order->save();

            $saved_clients[] = $client_order->id;
        }

        ClientOrder::where('order_id', $order->id)->whereNotIn('id', $saved_clients )->delete();

        $saved_accomodations = [];

        if( isset( $input['accomodations'] ) && isset( $input['accomodations']['country'] ) ){

            foreach ( $input['accomodations']['country'] as $index => $country_id ){
                $country = Country::find( $country_id );
                if( !$country ){
                    continue;
                }

                $town = null;
                $hotel = null;
                $acc   = null;
                $food  = null;
                $date_from = '';
                $date_to = '';
                $cost   = '';
                $count = '';

                if( isset( $input['accomodations']['town'][ $index ] )){
                    $town = Town::find( $input['accomodations']['town'][ $index ] );
                }

                if( isset( $input['accomodations']['hotel'][ $index ] )){
                    $hotel = Hotel::find( $input['accomodations']['hotel'][ $index ] );
                }

                if( isset( $input['accomodations']['acc'][ $index ] )){
                    $acc = Accomodation::find( $input['accomodations']['acc'][ $index ] );
                }

                if( isset( $input['accomodations']['food'][ $index ] )){
                    $food = Hotel::find( $input['accomodations']['food'][ $index ] );
                }

                if( isset( $input['accomodations']['date_from'][ $index ] )){
                    $date_from = $input['accomodations']['date_from'][ $index ];
                }

                if( isset( $input['accomodations']['date_to'][ $index ] )){
                    $date_to =  $input['accomodations']['date_to'][ $index ] ;
                }

                if( isset( $input['accomodations']['cost'][ $index ] )){
                    $cost = $input['accomodations']['cost'][ $index ];
                }

                if( isset( $input['accomodations']['count'][ $index ] )){
                    $count =  $input['accomodations']['count'][ $index ] ;
                }

                $acc_order = AccomodationOrder::where('order_id', $order->id )->skip( ( $index -1 ) )->take( 1 )->first();

                if( !$acc_order ){
                    $acc_order = new AccomodationOrder();
                }

                $acc_order->fill( [
                    'order_id'  => $order->id,
                    'country_id' => $country_id,
                    'town_id'   => $town ? $town->id : 0,
                    'hotel_id'  => $hotel ? $hotel->id : 0,
                    'accomodation_id' => $acc ? $acc->id : 0,
                    'food_id'   => $food ? $food->id : 0,
                    'date_from' => Carbon::parse($date_from),
                    'date_to'   => Carbon::parse($date_to),
                    'cost'      => $cost,
                    'count'     => $count
                ] );

                $acc_order->save();

                $saved_accomodations[] = $acc_order->id;

            }
        }

        AccomodationOrder::where('order_id', $order->id)->whereNotIn('id', $saved_accomodations )->delete();

        $saved_transfer = [];

        if( isset( $input['transfer'] ) && isset( $input['transfer']['town'] ) ){

            foreach ( $input['transfer']['town'] as $index => $town_id ){

                if( $town_id >0 ){

                    $transfer = Transfer::where('order_id', $order->id )->skip( ( $index - 1 ) )->take( 1 )->first();

                    if( !$transfer ){
                        $transfer = new Transfer();
                    }

                    $transfer->fill( [
                        'order_id'  => $order->id,
                        'town_id' => $town_id,
                        'transfer' => isset( $input['transfer']['transfer'][$index] ) ? $input['transfer']['transfer'][$index] : 0,
                        'count'   => isset( $input['transfer']['count'][$index] ) ? $input['transfer']['count'][$index] : 0,
                        'type'  => isset( $input['transfer']['type'][$index] ) ? $input['transfer']['type'][$index] : 0,
                    ] );

                    $transfer->save();

                    $saved_transfer[] = $transfer->id;
                }

            }
        }

        Transfer::where('order_id', $order->id)->whereNotIn('id', $saved_transfer )->delete();

        $saved_tickets = [];

        if( isset( $input['tickets'] ) && isset( $input['tickets']['type'] ) ){

            foreach ( $input['tickets']['type'] as $index => $type_id ){

                if( (int) $input['tickets']['count'][$index] > 0 ) {

                    $ticket = Ticket::where('order_id', $order->id)->skip(($index - 1))->take(1)->first();

                    if (!$ticket) {
                        $ticket = new Ticket();
                    }

                    $ticket->fill([
                        'order_id' => $order->id,
                        'town_from' => isset($input['tickets']['town_from'][$index]) ? (int) $input['tickets']['town_from'][$index] : 0,
                        'town_to' => isset($input['tickets']['town_to'][$index]) ? (int) $input['tickets']['town_to'][$index] : 0,
                        'type' => $type_id,
                        'date' => (int)isset($input['tickets']['date'][$index]) ? $input['tickets']['date'][$index] : null,
                        'count' => (int)isset($input['tickets']['count'][$index]) ? (int) $input['tickets']['count'][$index] : 0,
                        'cost' => (int) isset($input['tickets']['cost'][$index]) ? (int) $input['tickets']['cost'][$index] : 0,
                    ]);

                    $ticket->save();

                    $saved_tickets[] = $ticket->id;
                }
            }
        }

        Ticket::where('order_id', $order->id)->whereNotIn('id', $saved_tickets )->delete();

        $saved_insurance = [];

        if( isset( $input['insurance'] ) && isset( $input['insurance']['type'] ) ){

            foreach ( $input['insurance']['type'] as $index => $type_id ){

                if( $type_id >0 ) {

                    $insurance = Insurance::where('order_id', $order->id)->skip(($index - 1))->take(1)->first();

                    if (!$insurance) {
                        $insurance = new Insurance();
                    }

                    $insurance->fill([
                        'order_id' => $order->id,
                        'type' => $type_id,
                        'count' => isset($input['insurance']['count'][$index]) ? (int) $input['insurance']['count'][$index] : 0,
                        'cost' => isset($input['insurance']['cost'][$index]) ? (int) $input['insurance']['cost'][$index] : 0,
                    ]);

                    $insurance->save();

                    $saved_insurance[] = $insurance->id;
                }

            }
        }

        Insurance::where('order_id', $order->id)->whereNotIn('id', $saved_insurance )->delete();

        $saved_visas = [];

        if( isset( $input['visas'] ) && isset( $input['visas']['country'] ) ){

            foreach ( $input['visas']['country'] as $index => $country_id ){
                $country = Country::find( $country_id );
                if( !$country ){
                    continue;
                }

                $visa = Visa::where('order_id', $order->id)->skip(($index - 1))->take(1)->first();

                if (!$visa) {
                    $visa = new Visa();
                }


                $visa->fill( [
                    'order_id'  => $order->id,
                    'country_id' => $country_id,
                    'count'   => isset( $input['visas']['count'][$index] ) ? $input['visas']['count'][$index] : 0,
                    'cost'  => isset( $input['visas']['cost'][$index] ) ? $input['visas']['cost'][$index] : 0,
                ] );

                $visa->save();

                $saved_visas[] = $visa->id;

            }
        }

        Visa::where('order_id', $order->id)->whereNotIn('id', $saved_visas )->delete();

        $saved_services = [];

        if( isset( $input['services'] ) && isset( $input['services']['name'] ) ){

            foreach ( $input['services']['name'] as $index => $name ){

                if( strlen( $name ) > 0 ) {

                    $service = OrderService::where('order_id', $order->id)->skip(($index - 1))->take(1)->first();

                    if (!$service) {
                        $service = new OrderService();
                    }

                    $service->fill([
                        'order_id' => $order->id,
                        'name' => $name,
                        'count' => isset($input['services']['count'][$index]) ? $input['services']['count'][$index] : 0,
                        'cost' => isset($input['services']['cost'][$index]) ? $input['services']['cost'][$index] : 0,
                    ]);

                    $service->save();

                    $saved_services[] = $service->id;

                }
            }
        }

        OrderService::where('order_id', $order->id)->whereNotIn('id', $saved_services )->delete();


        $saved_client_payments = [];

        if( isset( $input['client_payments'] ) && isset( $input['client_payments']['amount'] ) ){

            foreach ( $input['client_payments']['amount'] as $index => $amount ){

                if( (int) $amount > 0 ) {

                    $payment = ClientPayment::where('order_id', $order->id)->skip(($index - 1))->take(1)->first();

                    if (!$payment) {
                        $payment = new ClientPayment();
                    }

                    $payment->fill([
                        'order_id' => $order->id,
                        'amount' => $amount,
                        'sign' => isset( $input['client_payments']['sign'][$index]) ? $input['client_payments']['sign'][$index] : 0,
                        'type' => isset( $input['client_payments']['type'][$index]) ? $input['client_payments']['type'][$index] : 0,
                        'date' => isset( $input['client_payments']['date'][$index] ) ? $input['client_payments']['date'][$index] : '',
                    ]);

                    $payment->save();

                    $saved_client_payments[] = $payment->id;

                }
            }
        }

        ClientPayment::where('order_id', $order->id)->whereNotIn('id', $saved_client_payments )->delete();

        $saved_client_payments = [];

        if( isset( $input['confirmations'] ) && isset( $input['confirmations']['amount_ue'] ) ){

            foreach ( $input['confirmations']['amount_ue'] as $index => $amount ){

                if( (int) $amount > 0 ) {

                    $confirmation = OrderConfirmation::where('order_id', $order->id)->skip(($index - 1))->take(1)->first();

                    if (!$confirmation) {
                        $confirmation = new OrderConfirmation();
                    }

                    $confirmation->fill([
                        'order_id' => $order->id,
                        'amount_ue' => $amount,
                        'currency' => isset( $input['confirmations']['currency'][$index]) ? $input['confirmations']['currency'][$index] : 0,
                        'date' => isset( $input['confirmations']['date'][$index]) ? $input['confirmations']['date'][$index] : null,
                        'number' => isset( $input['confirmations']['number'][$index]) ? $input['confirmations']['number'][$index] : 0,
                    ]);

                    $confirmation->save();

                    $saved_client_payments[] = $confirmation->id;

                }
            }
        }

        OrderConfirmation::where('order_id', $order->id)->whereNotIn('id', $saved_client_payments )->delete();


        $saved_tourop = [];

        if( isset( $input['tourop_payments'] ) && isset( $input['tourop_payments']['amount'] ) ){

            foreach ( $input['tourop_payments']['amount'] as $index => $amount ){

                if( (int) $amount > 0 ) {

                    $payment = TouropPayment::where('order_id', $order->id)->skip(($index - 1))->take(1)->first();

                    if (!$payment) {
                        $payment = new TouropPayment();
                    }

                    $payment->fill([
                        'order_id' => $order->id,
                        'amount' => $amount,
                        'sign' => isset( $input['tourop_payments']['sign'][$index]) ? $input['tourop_payments']['sign'][$index] : 0,
                        'exchange_rate' => isset( $input['tourop_payments']['exchange_rate'][$index]) ? $input['tourop_payments']['exchange_rate'][$index] : 0,
                        'amount_ue' => isset( $input['tourop_payments']['amount_ue'][$index]) ? $input['tourop_payments']['amount_ue'][$index] : 0,
                        'type' => isset( $input['tourop_payments']['type'][$index]) ? $input['tourop_payments']['type'][$index] : 0,
                        'date' => isset( $input['tourop_payments']['date'][$index]) ? $input['tourop_payments']['date'][$index] : 0,
                    ]);

                    $payment->save();

                    $saved_tourop[] = $payment->id;

                }
            }
        }

        TouropPayment::where('order_id', $order->id)->whereNotIn('id', $saved_tourop )->delete();



        return response()->json( [
            'message' => 'Сохранено',
            'order_id' => $order->id
        ] );
    }

    public function getClientRow( $id ){

        $client = Client::find( $id );
        $tourist = new ClientOrder();

        $tourist->setRelation('client',$client);

        return view( 'orders/tourist_row', [ 'tourist' => $tourist ] );
    }

    public function Contract( $id ){

        $order = Order::find( $id );

        if( !$order ){
            abort( 404 );
        }

        return view( 'contract/contract', [ 'order' => $order ] );
    }

    public function App1( $id ){
        $order = Order::find( $id );

        if( !$order ){
            abort( 404 );
        }

        return view( 'contract/app1', [ 'order' => $order ] );
    }

    public function App2( $id ){
        $order = Order::find( $id );

        if( !$order ){
            abort( 404 );
        }

        return view( 'contract/app2', [ 'order' => $order ] );
    }


}
