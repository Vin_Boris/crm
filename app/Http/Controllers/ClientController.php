<?php namespace App\Http\Controllers;

use App\Http\Models\Client;
use App\Http\Models\User;
use Illuminate\Http\Request;
use Carbon\Carbon;

class ClientController extends Controller {

    public function index(){

        $filter = [
            'last_name'         => \Input::get('last_name', ''),
            'first_name'        => \Input::get('first_name', '' ),
            'middle_name'       => \Input::get('middle_name', '' ),
            'tour_date_from'    => \Input::get('tour_date_from', null ),
            'tour_date_to'      => \Input::get('tour_date_to', null ),
            'order_date_from'   => \Input::get('order_date_from', null),
            'order_date_to'     => \Input::get('order_date_to', null),
            'user_id'           => \Input::get('user_id', 0),
            'country_id'        => \Input::get('country_id', 0),
            'country_name'      => \Input::get('country_name', ''),
        ];

        $clients = Client::orderBy('id','DESC');

        if( $filter[ 'last_name' ] != '' ){
            $clients->where( 'last_name', 'LIKE', '%' . $filter[ 'last_name' ] . '%' );
        }

        if( $filter[ 'first_name' ] != '' ){
            $clients->where( 'first_name', '%' . $filter[ 'first_name' ] . '%' );
        }

        if( $filter[ 'middle_name' ] != '' ){
            $clients->where( 'middle_name', '%' . $filter[ 'middle_name' ] . '%' );
        }

        if(  $filter[ 'order_date_from' ] &&  Carbon::createFromFormat( 'd.m.Y', $filter[ 'order_date_from' ] )->timestamp > 0 ){

            $clients->whereHas( 'orders.order', function( $q ) use( $filter ){
                $q->whereDate( 'created_at', '>=', Carbon::createFromFormat( 'd.m.Y', $filter[ 'order_date_from' ] )->startOfDay()->toDateTimeString() );
            });

        }

        if(  $filter[ 'order_date_to' ] &&  Carbon::createFromFormat( 'd.m.Y', $filter[ 'order_date_to' ] )->timestamp > 0 ){
            $clients->whereHas( 'orders.order', function( $q ) use( $filter ){
                $q->whereDate( 'created_at', '<=', Carbon::createFromFormat( 'd.m.Y', $filter[ 'order_date_to' ] )->endOfDay()->toDateTimeString() );
            });
        }


        if(  $filter[ 'tour_date_from' ] && Carbon::createFromFormat( 'd.m.Y', $filter[ 'tour_date_from' ] )->timestamp > 0 ){
            $clients->whereHas('orders', function( $q ) use( $filter ){
                $q->whereDate( 'date_from', '>=', Carbon::createFromFormat( 'd.m.Y', $filter[ 'tour_date_from' ] )->startOfDay()->toDateTimeString() );
            });
        }

        if(  $filter[ 'tour_date_to' ] && Carbon::createFromFormat( 'd.m.Y', $filter[ 'tour_date_to' ] )->timestamp > 0 ){
            $clients->whereHas('clients', function( $q ) use( $filter ){
                $q->whereDate( 'date_to', '>=', Carbon::createFromFormat( 'd.m.Y', $filter[ 'tour_date_to' ] )->endOfDay()->toDateTimeString() );
            });
        }

        if( (int) $filter[ 'country_id' ] >0 ){

            $country =  Country::find( $filter[ 'country_id' ] );

            if( $country ){
                $clients->whereHas('orders.order.accomodations', function( $q ) use( $filter ){
                    $q->where( 'country_id', $filter[ 'country_id' ] );
                });

                $filter['country_name'] =$country->name;
            }


        }

        if( (int) $filter[ 'user_id' ] >0 ){
            $clients->where( 'user_id', $filter[ 'user_id' ] );
        }



        $add_to_order = false;
        $add_to_order_contract = false;

        if( \Input::has('add_to_order') ){
            $add_to_order = \Input::get('add_to_order');
        }

        if( \Input::has('add_to_order_contract') ){
            $add_to_order_contract = \Input::get('add_to_order_contract');
        }

        return view( 'clients/list', [
            'data' => $clients->get() ,
            'add_to_order' => $add_to_order,
            'add_to_order_contract' => $add_to_order_contract,
            'filter'  => $filter,
            'users'   => User::get()
        ] );
    }

    public function edit( $id = 0){

        $client = Client::find( $id );

        if( !$client ){
            $client = new Client();
            if( !empty( \Input::old() ) ) {
                self::fillClientFromInput($client, \Input::old() );
            }
        }

        return view( 'clients/form', [ 'client' => $client ] );
    }

    public function info( $id = 0){

        $client = Client::find( $id );

        if( !$client ){
           abort( 404 );
        }

        return response()->json( [
            'fullname' => $client->fullname(),
            'birthday' => \Carbon\Carbon::parse( $client->birthday )->format( 'd.m.Y' )
        ] );
    }

    public function save( Request $request , $id = 0 ){
        $this->validate($request, [
            'first_name'             => 'required|max:255',
            'last_name'              => 'required|max:255',
            'int_passport_till'      => 'date',
            'visa_till'              => 'date'
        ], [
            'first_name.required' => 'Имя обязательно для заполнения',
            'last_name.required' => 'Фимилия обязательно для заполнения',
            'int_passport_till.date' => 'Неверный формат даты окончания действия загранпаспорта',
            'visa_till.date' => 'Неверный формат даты окончания действия визы'
        ]);

        $post = \Input::all();

        $client = Client::find( $id );

        if( !$client ) {
            $client = new Client();
        }

        self::fillClientFromInput( $client, $post );

        $client->save();

        return response()->redirectTo( "/clients"  );
    }

    private static function fillClientFromInput( &$client, $post ){

        $client->user_id = \Auth::user()->id;
        $client->office_id = \Auth::user()->office_id;

        $client->first_name = $post['first_name'];
        $client->last_name = $post['last_name'];
        $client->middle_name = $post['middle_name'];

        $client->first_name_eng = $post['first_name_eng'];
        $client->last_name_eng = $post['last_name_eng'];

        $client->phone = $post['phone'];
        $client->email = $post['email'];

        $client->international_passport = $post['international_passport'];

        $client->int_passport_till =  $post['int_passport_till'];


        $client->international_passport_gift = $post['international_passport_gift'];

        $client->passport = $post['passport'];
        $client->passport_gift = $post['passport_gift'];

        $client->birthday =  $post['birthday'];
        $client->sex = $post['sex'];

        if( isset( $post['have_visa'] )){
            $client->have_visa = 1;
        }else{
            $client->have_visa = 0;
        }

        $client->visa_till =$post['visa_till'];

        $client->type = $post['type'];
    }

}
