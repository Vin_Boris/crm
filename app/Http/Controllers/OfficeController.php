<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Models\LegalEntity;
use App\Http\Models\Office;
use Illuminate\Http\Request;

class OfficeController extends Controller {

    public function index(){
        $offices = Office::get();

        return view( 'offices/list', [ 'data' => $offices ] );
    }

    public function edit( $id = 0){

        $office = Office::find( $id );

        if( !$office ){
            $office = new Office();
            if( !empty( \Input::old() ) ) {
                self::fillOfficeFromPost($office, \Input::old());
            }
        }

        $legal_entities = LegalEntity::get();

        return view( 'offices/form', [ 'office' => $office,  'legal_entities' => $legal_entities ] );
    }

    public function save( Request $request , $id = 0 ){

        $this->validate($request, [
            'name'         => 'required|max:255',
            'address'      => 'required|max:255',
            'phone'        => 'required|max:255',
            'email'        => 'required|email',
        ],[
            'name.required' => 'Название офиса обязательно для заполнения',
            'address.required' => 'Адрес обязательно для заполнения',
            'phone.required' => 'Телефон обязательно для заполнения',
            'email.required' => 'Email обязательно для заполнения',
        ]);

        $post = \Input::all();

        $office = Office::find( $id );

        if( !$office ) {
            $office = new Office();
        }

        self::fillOfficeFromPost( $office, $post );

        $office->save();

        return response()->redirectTo( "/offices/edit/" .  $office->id  );
    }

    private static function fillOfficeFromPost( &$office, $post ){

        $office->name = $post['name'];
        $office->jurname_id = (int) $post['legal_entity'];
        $office->address = $post['address'];
        $office->phone = $post['phone'];
        $office->email = $post['email'];
    }

}
