<?php namespace App\Http\Controllers;


use App\Http\Models\LegalEntity;
use App\Http\Models\LegalEntityAccount;
use App\Http\Models\LegalForm;
use Illuminate\Http\Request;

class LegalEntityController extends Controller {

    public function index(){
        $legal_entities = LegalEntity::with('legal_form')->get();

        return view( 'legal_entity/list', [ 'legal_entities' => $legal_entities ] );
    }

    public function edit( Request $request,$id = 0){

        $legal_entity = LegalEntity::find( $id );

        if( !$legal_entity ){
            $legal_entity = new LegalEntity();
            if( !empty( \Input::old() ) ) {
                self::setLegalEntityAttributesFromInput($legal_entity, \Input::old());
            }
        }

        $legal_forms = LegalForm::get();
        $accounts = $legal_entity->accounts()->get();

        return view( 'legal_entity/form', [ 'legal_entity' => $legal_entity,  'legal_forms' => $legal_forms, 'accounts' => $accounts  ] );
    }

    public function save( Request $request, $id = 0 ){

        $this->validate($request, [
            'name'         => 'required|max:255'
        ],[
            'name.required' => 'Название обязательно для заполнения'
        ]);

        $post = \Input::all();

        $legal_entity = LegalEntity::find( $id );

        if( !$legal_entity ) {
            $legal_entity = new LegalEntity();
        }

        self::setLegalEntityAttributesFromInput( $legal_entity, $post );

        $legal_entity->save();

        return response()->redirectTo( "/legal_entities" );
    }

    public function accountEdit( $entity_id , $id = 0){

        $entity_account = LegalEntityAccount::find( $id );

        if( !$entity_account ){
            $entity_account = new LegalEntityAccount();

            if( !empty( \Input::old() ) ){
                self::setLegalEntityAccountFromInput( $entity_account, $entity_id,  \Input::old() );
            }
        }

        $entity_account->jurname_id = $entity_id;

        return view( 'legal_entity/account_form', [ 'entity_account' => $entity_account ] );
    }

    public function accountSave( Request $request, $entity_id, $id = 0 ){

        $this->validate($request, [
            'rs'         => 'required|max:255',
            'bik'       => 'required|max:255'
        ],[
            'rs.required' => 'Р\с обязательно для заполнения',
            'bik.required' => 'БИК обязательно для заполнения'
        ]);

        $post = \Input::all();

        $entity_account = LegalEntityAccount::find( $id );

        if( !$entity_account ) {
            $entity_account = new LegalEntityAccount();

        }

        self::setLegalEntityAccountFromInput( $entity_account, $entity_id,  $post );

        $entity_account->save();

        return response()->redirectTo( "/legal_entities/accounts/edit/" . $entity_account->jurname_id . '/' . $entity_account->id );
    }

    private static function setLegalEntityAttributesFromInput( &$legal_entity, $input ){

        $legal_entity->name = $input['name'];
        $legal_entity->orgform_id = $input['legal_from'];
        $legal_entity->address = $input['address'];
        $legal_entity->phone = $input['phone'];
        $legal_entity->director_fio = $input['director_fio'];
        $legal_entity->ogrn = $input['ogrn'];
        $legal_entity->town = $input['town'];
        $legal_entity->okpo = $input['okpo'];
        $legal_entity->okato = $input['okato'];
    }

    private static function setLegalEntityAccountFromInput( &$entity_account, $entity_id, $input ){

        $entity_account->jurname_id = $entity_id;
        $entity_account->rs = $input['rs'];
        $entity_account->bik = $input['bik'];
        $entity_account->bank = $input['bank'];
        $entity_account->ks = $input['ks'];
    }

}
