<?php namespace App\Http\Controllers;

use App\Http\Models\LegalForm;
use Illuminate\Http\Request;

class LegalFormController extends Controller {

    public function index(){

        $legal_forms = LegalForm::get();

        return view( 'legal_form/list', [ 'data' => $legal_forms ] );
    }

    public function edit( $id = 0 ){

        $legal_form = LegalForm::find( $id );

        if( !$legal_form ){
            $legal_form = new LegalForm();
        }

        return view( 'legal_form/form', [ 'legal_form' => $legal_form ] );
    }

    public function save(Request $request, $id = 0 ){

        $this->validate($request, [
            'name'         => 'required|max:255',
        ], [
            'name.required' => 'Название обязательно для заполнения'
        ]);

        $post = \Input::all();

        $legal_form = LegalForm::find( $id );

        if( !$legal_form ) {
            $legal_form = new LegalForm();
        }

        $legal_form->name = $post['name'];

        $legal_form->save();

        return response()->redirectTo( "/legal_forms" );
    }

}
