<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Office;
use App\Http\Models\User;

class UserController extends Controller {

    public function index(){
        $users = User::get();

        return view( 'users/list', [ 'data' => $users ] );
    }

    public function edit( $id = 0){

        $user = User::find( $id );

        if( !$user ){
            $user = new User();
            if( !empty( \Input::old() ) ) {
                self::fillUserFromInput($user, \Input::old());
            }
        }

        return view( 'users/form', [ 'user' => $user,  'offices' => Office::get()  ] );
    }

    public function save(Request $request , $id = 0 ){

        $post = \Input::all();

        $this->validate($request, [
            'firstname'             => 'required|max:255',
            'lastname'              => 'required|max:255',
        ], [
            'firstname.required' => 'Имя обязательно для заполнения',
            'lastname.required' => 'Фимилия обязательно для заполнения',
        ]);

        $user = User::find( $id );

        if( !$user ) {
            $user = new User();
        }

        self::fillUserFromInput( $user, $post );

        $user->save();

        return response()->redirectTo( "/users" );
    }

    private function fillUserFromInput( &$user, $post ){

        $user->first_name = $post['firstname'];
        $user->last_name = $post['lastname'];
        $user->middle_name = $post['middlename'];
        $user->email = $post['login'];
        $user->password = $post['password'];
        $user->office_id = $post['office'];
        $user->role = 0;
        $user->email = $post['email'];
        $user->phone = $post['phone'];
        $user->birthday = $post['birthdate'];
        $user->sex = $post['sex'];
        $user->isactive = isset( $post['isactive'] ) ? 1  : 0 ;
    }

}
