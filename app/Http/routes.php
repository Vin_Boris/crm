<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/



Route::get('/', 'Auth\AuthController@index');
Route::post('/', 'Auth\AuthController@postLogin');
Route::get('/logout', 'Auth\AuthController@getLogout');

Route::group(['middleware' => 'auth'], function () {

    Route::get('/legal_forms', 'LegalFormController@index');
    Route::get('/legal_forms/edit/{id?}', 'LegalFormController@edit');
    Route::post('/legal_forms/save', 'LegalFormController@save');

    Route::get('/legal_entities', 'LegalEntityController@index');
    Route::get('/legal_entities/edit/{id?}', 'LegalEntityController@edit');
    Route::post('/legal_entities/save/{id?}', 'LegalEntityController@save');

    Route::get('/legal_entities/accounts/edit/{entity_id}/{id?}', 'LegalEntityController@accountEdit');
    Route::post('/legal_entities/accounts/save/{entity_id}/{id?}', 'LegalEntityController@accountSave');

    Route::get('/offices', 'OfficeController@index');
    Route::get('/offices/edit/{id?}', 'OfficeController@edit');
    Route::post('/offices/save/{id?}', 'OfficeController@save');

    Route::get('/users', 'UserController@index');
    Route::get('/users/edit/{id?}', 'UserController@edit');
    Route::post('/users/save/{id?}', 'UserController@save');

    Route::get('/tour_operators', 'TouropController@index');
    Route::get('/tour_operators/edit/{id?}', 'TouropController@edit');
    Route::post('/tour_operators/save/{id?}', 'TouropController@save');

    Route::get('/countries', 'DictionaryController@countries' );
    Route::post('/countries/edit/{id}', 'DictionaryController@countriesEdit');

    Route::get('/towns/{country_id?}', 'DictionaryController@towns' );
    Route::post('/towns/edit/{country_id}/{id?}', 'DictionaryController@townsEdit');

    Route::get('/hotels/{town_id}', 'DictionaryController@hotels' );
    Route::post('/hotels/edit/{town_id}/{id?}', 'DictionaryController@hotelsEdit');

    Route::get('/food', 'DictionaryController@food'  );
    Route::post('/food/edit/{id?}', 'DictionaryController@foodEdit');

    Route::get('/accomodations', 'DictionaryController@accomodations'  );
    Route::post('/accomodations/edit/{id?}', 'DictionaryController@accomodationsEdit');


    Route::get('/clients', 'ClientController@index');
    Route::get('/clients/edit/{id?}', 'ClientController@edit');
    Route::post('/clients/save/{id?}', 'ClientController@save');
    Route::get('/clients/info/{id?}', 'ClientController@info');

    Route::get('/orders', 'OrderController@Index');
    Route::get('/orders/edit/{id?}', 'OrderController@Edit');
    Route::post('/orders/save/{id?}', 'OrderController@Save');
    Route::get('/orders/get-client-row/{id}', 'OrderController@getClientRow');

    Route::get('/orders/contract/{id}', 'OrderController@Contract');
    Route::get('/orders/contract-app-1/{id}', 'OrderController@App1');
    Route::get('/orders/contract-app-2/{id}', 'OrderController@App2');


    Route::get('/report', 'ReportController@index');
});
