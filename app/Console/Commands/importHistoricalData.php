<?php namespace App\Console\Commands;

use App\Http\Models\LegalEntity;
use Illuminate\Console\Command;

class importHistoricalData extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'unitours:import';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		//

        $path = base_path() . '/database/old_data/y_jurnames.sql';
        \DB::unprepared(file_get_contents($path));

        $jurnames = \DB::select( 'SELECT * FROM y_jurnames' );
        \DB::delete('DELETE FROM legal_entities');

        foreach ( $jurnames as $jurname ){

        }

	}

}
