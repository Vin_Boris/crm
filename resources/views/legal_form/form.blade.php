@extends('layouts.dashboard')

@section('page_heading','Добавление/редактирование орг-правовой формы')

@section('section')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="col-sm-12">
            <form method="POST" action="{{ url ('/legal_forms/save' . ( $legal_form->id > 0 ? '/' . $legal_form->id : '' ) ) }}">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                 <div class="form-group">
                    <label>Название</label>
                    <input class="form-control" name="name" value="{{ $legal_form->name }}">

                </div>

                <button type="submit" class="btn btn-default">Сохранить</button>

            </form>
        </div>
    </div>

@endsection