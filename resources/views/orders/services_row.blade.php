<div class="service_row">
    <div class="col-sm-8">

        <input type="text" class="form-control" name="services[name][]" value="{{ $service->name }}">
    </div>

    <div class="col-sm-2">
        <input type="number" class="form-control" name="services[cost][]" value="{{ $service->cost }}" >
    </div>

    <div class="col-sm-1">
        <input type="number" class="form-control" name="services[count][]" value="{{ $service->count }}" >
    </div>

    <div class="col-sm-1" align="center">
        <button type="button" class="btn btn-danger btn-circle remove_service"><i class="fa fa-remove"></i></button>
    </div>

    <div class="clearfix"></div>

</div>