
<div class="visas_row"   >

    <div class="col-sm-3">

        <div class="form-group input-group ">
            <input class="form-control"  type="hidden" name="visas[country][]"  value="{{ $visa->country_id }}" />
            <input class="form-control visas_country_autocomplete" type="text"   value="{{ $visa->country ? $visa->country->name : '' }}" placeholder="Введите страну"/>
            <span class="input-group-btn" >
                    <button type="button" class="btn btn-default "><i class="fa fa-check"></i></button>
            </span>
        </div>

    </div>
    <div class="col-sm-5">
        <input type="text" class="form-control"  name="visas[description][]" value="{{ $visa->description }}" >
    </div>

    <div class="col-sm-2">
        <input type="number" class="form-control"  name="visas[cost][]" value="{{ $visa->cost }}" >
    </div>

    <div class="col-sm-1">
        <input type="number" class="form-control"  name="visas[count][]" value="{{ $visa->count }}" >
    </div>

    <div class="col-sm-1" align="center">
        <button type="button" class="btn btn-danger btn-circle remove_visa"><i class="fa fa-remove"></i></button>
    </div>

    <div class="clearfix"></div>

</div>