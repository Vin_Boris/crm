<div class="ticket_row"   >


    <div class="col-sm-3">

        <div class="form-group input-group ">
            <input class="form-control"  type="hidden" name="tickets[town_from][]"  value="{{ $ticket->town_from }}" />
            <input class="form-control ticket_town_autocomplete" type="text"   value="{{ $ticket->from_town ?$ticket->from_town->name : '' }}" placeholder="Введите город" />
            <span class="input-group-btn" >
                    <button type="button" class="btn btn-default "><i class="fa fa-check"></i></button>
            </span>
        </div>
    </div>


    <div class="col-sm-3">

        <div class="form-group input-group ">
            <input class="form-control"  type="hidden" name="tickets[town_to][]"  value="{{ $ticket->town_to }}" />
            <input class="form-control ticket_town_autocomplete" type="text"   value="{{ $ticket->to_town ?$ticket->to_town->name : '' }}" placeholder="Введите город" />
            <span class="input-group-btn" >
                    <button type="button" class="btn btn-default "><i class="fa fa-check"></i></button>
            </span>
        </div>
    </div>

    <div class="col-sm-2">

        <select class="form-control" name="tickets[type][]">
            <option value="0" >Не выбрано</option>
            <option value="{{\App\Http\Models\Ticket::AVIA_ECONOM}}" {{ $ticket->type == \App\Http\Models\Ticket::AVIA_ECONOM ? 'selected' : '' }}>Авиаэконом</option>
            <option value="{{\App\Http\Models\Ticket::RAILWAY}}" {{ $ticket->type == \App\Http\Models\Ticket::RAILWAY ? 'selected' : '' }} >ЖД</option>
        </select>
    </div>

    <div class="col-lg-1" >
        <input type="text" class="form-control datepicker_input"  name="tickets[date][]"  value="{{ $ticket->date }}" >
    </div>

    <div class="col-lg-1" >
        <input type="number" class="form-control "  name="tickets[cost][]"  value="{{ $ticket->cost }}" >
    </div>

    <div class="col-lg-1" >

        <input type="number" class="form-control" name="tickets[count][]"  value="{{ $ticket->count }}">

    </div>

    <div class="col-sm-1" align="center">
        <button type="button" class="btn btn-danger btn-circle remove_ticket"><i class="fa fa-remove"></i></button>
    </div>

    <div class="clearfix"></div>

</div>