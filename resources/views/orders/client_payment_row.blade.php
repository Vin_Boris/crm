<div class="client_payment_row"   >


    <div class="col-sm-1">
        <select class="form-control" name="client_payments[sign][]">
            <option value="+" {{ $payment->sign == '+' ? 'selected' : ''  }}>+</option>
            <option value="-" {{ $payment->sign == '-' ? 'selected' : ''  }}>-</option>
        </select>
    </div>

    <div class="col-sm-3">
        <input type="number" class="form-control" name="client_payments[amount][]" value="{{  $payment->amount }}">
    </div>

    <div class="col-sm-3">
        <select class="form-control" name="client_payments[type][]">
            <option value="{{ \App\Http\Models\ClientPayment::TYPE_CASH }}" {{ $payment->type == \App\Http\Models\ClientPayment::TYPE_CASH ? 'selected' : ''  }}>Наличные</option>
            <option value="{{ \App\Http\Models\ClientPayment::TYPE_NON_CASH }}" {{ $payment->type == \App\Http\Models\ClientPayment::TYPE_NON_CASH ? 'selected' : ''  }}>Безнал</option>
        </select>
    </div>

    <div class="col-sm-2">
        <input type="text" class="form-control datepicker_input"  name="client_payments[date][]"  value="{{ $payment->date }}" >
    </div>

    <div class="col-sm-2">
       <a href="#" >Скачать</a>
    </div>

    <div class="col-sm-1" align="center">
        <button type="button" class="btn btn-danger btn-circle remove_client_payment"><i class="fa fa-remove"></i></button>
    </div>

    <div class="clearfix"></div>

</div>