<div class="transfer_row"   >

    <div class="col-sm-4">

        <div class="form-group input-group ">
            <input class="form-control"  type="hidden" name="transfer[town][]"  value="{{ $transfer->town_id }}" />
            <input class="form-control transfer_town_autocomplete" type="text"   value="{{ $transfer->town ? $transfer->town->name : '' }}" placeholder="Введите город" />
            <span class="input-group-btn" >
                    <button type="button" class="btn btn-default "><i class="fa fa-check"></i></button>
            </span>
        </div>
    </div>

    <div class="col-sm-3">

        <select class="form-control" name="transfer[transfer][]">
            <option value="0" >Не выбрано</option>
            <option value="{{\App\Http\Models\Transfer::AIRPORT_HOTEL}}" {{ $transfer->transfer == \App\Http\Models\Transfer::AIRPORT_HOTEL ? 'selected' : '' }}>Аэропорт отель</option>
            <option value="{{\App\Http\Models\Transfer::HOTEL_AIRPORT}}" {{ $transfer->transfer == \App\Http\Models\Transfer::HOTEL_AIRPORT ? 'selected' : '' }}>Отель аэропорт</option>
        </select>
    </div>

    <div class="col-sm-2">

        <select class="form-control" name="transfer[type][]">
            <option value="0" >Не выбрано</option>
            <option value="{{\App\Http\Models\Transfer::TYPE_GROUP}}" {{ $transfer->type == \App\Http\Models\Transfer::TYPE_GROUP ? 'selected' : '' }} >Групповой</option>
            <option value="{{\App\Http\Models\Transfer::TYPE_INDIVIDUAL}}" {{ $transfer->type == \App\Http\Models\Transfer::TYPE_INDIVIDUAL ? 'selected' : '' }} >Индивидуальный</option>
        </select>
    </div>

    <div class="col-lg-2" >

        <input type="number" class="form-control" name="transfer[count][]" value="{{ $transfer->count }}">

    </div>


    <div class="col-sm-1" align="center">
        <button type="button" class="btn btn-danger btn-circle remove_transfer"><i class="fa fa-remove"></i></button>
    </div>

    <div class="clearfix"></div>

</div>