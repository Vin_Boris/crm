
<div data-client="{{ $tourist->client->id }}" class="tourist_row" >
    <div class="col-sm-8">
        {{ $tourist->client->fullname() }}
    </div>
    <div class="col-sm-3">

        <div class="row" >
            <div class="col-lg-6" >
                <div class="form-group input-group ">
                    <span class="input-group-addon">С</span>
                    <input type="text" class="date_from form-control datepicker_input" name="clients[{{ $tourist->client->id }}][date_from]" value="{{ \Carbon\Carbon::parse( $tourist->date_from )->format( 'd.m.Y' )  }}" >
                </div>
            </div>

            <div class="col-lg-6" >
                <div class="form-group input-group ">
                    <span class="input-group-addon">По</span>
                    <input type="text" class="date_to form-control datepicker_input" name="clients[{{ $tourist->client->id }}][date_to]" value="{{  \Carbon\Carbon::parse( $tourist->date_to )->format( 'd.m.Y' )}}" >
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-1" align="center">
        <button type="button" class="btn btn-danger btn-circle remove_client " data-remove_client="{{ $tourist->client->id }}" ><i class="fa fa-remove"></i></button>
    </div>

    <div class="clearfix"></div>

</div>
