<div class="confirmation_row"   >

    <div class="col-sm-2">
        <input type="text" class="form-control datepicker_input"  name="confirmations[date][]"  value="{{ $confirmation->date }}" >
    </div>

    <div class="col-sm-2">
        <input type="text" class="form-control "  name="confirmations[number][]"  value="{{ $confirmation->number }}" >
    </div>

    <div class="col-sm-3">
        <input type="text" class="form-control "  name="confirmations[amount_ue][]"  value="{{ $confirmation->amount_ue }}" >
    </div>

    <div class="col-sm-2">

        <select class="form-control" name="confirmations[currency][]">
            <option value="{{ \App\Http\Models\TouropPayment::RUB }}" {{ $confirmation->currency == \App\Http\Models\TouropPayment::RUB ? 'selected' : ''  }}>Рубли</option>
            <option value="{{ \App\Http\Models\TouropPayment::USD }}" {{ $confirmation->currency == \App\Http\Models\TouropPayment::USD ? 'selected' : ''  }}>Доллары</option>
            <option value="{{ \App\Http\Models\TouropPayment::EUR }}" {{ $confirmation->currency == \App\Http\Models\TouropPayment::EUR ? 'selected' : ''  }}>Евро</option>
        </select>

    </div>

    <div class="col-sm-2">
        @if( $confirmation->file )
            <a href="/orders/confirmation_file/{{ $confirmation->id }}" ></a>
        @endif
        <input type="file" class="form-control "  name="confirmations[file][]"   >
    </div>


    <div class="col-sm-1" align="center">
        <button type="button" class="btn btn-danger btn-circle remove_ticket"><i class="fa fa-remove"></i></button>
    </div>

    <div class="clearfix"></div>

</div>