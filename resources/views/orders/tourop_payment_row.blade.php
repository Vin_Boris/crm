<div class="tourop_payment_row"   >


    <div class="col-sm-1">
        <select class="form-control" name="tourop_payments[sign][]">
            <option value="+" {{ $payment->sign == '+' ? 'selected' : ''  }}>+</option>
            <option value="-" {{ $payment->sign == '-' ? 'selected' : ''  }}>-</option>
        </select>
    </div>

    <div class="col-sm-2">
        <input type="number" class="form-control" name="tourop_payments[amount][]" value="{{  $payment->amount }}">
    </div>

    <div class="col-sm-2">
        <input type="number" class="form-control" name="tourop_payments[exchange_rate][]" value="{{  $payment->exchange_rate }}">
    </div>


    <div class="col-sm-2">
        <input type="number" class="form-control" name="tourop_payments[amount_ue][]" value="{{  $payment->amount_ue }}">
    </div>



    <div class="col-sm-2">
        <select class="form-control" name="tourop_payments[type][]">
            <option value="{{ \App\Http\Models\TouropPayment::TYPE_CASH }}" {{ $payment->type == \App\Http\Models\TouropPayment::TYPE_CASH ? 'selected' : ''  }}>Наличные</option>
            <option value="{{ \App\Http\Models\TouropPayment::TYPE_NON_CASH }}" {{ $payment->type == \App\Http\Models\TouropPayment::TYPE_NON_CASH ? 'selected' : ''  }}>Безнал</option>
        </select>
    </div>

    <div class="col-sm-2">
        <input type="text" class="form-control datepicker_input"  name="tourop_payments[date][]"  value="{{ $payment->date }}" >
    </div>

    <div class="col-sm-1" align="center">
        <button type="button" class="btn btn-danger btn-circle remove_ticket"><i class="fa fa-remove"></i></button>
    </div>

    <div class="clearfix"></div>

</div>