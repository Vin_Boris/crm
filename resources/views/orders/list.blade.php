@extends('layouts.dashboard')

@section('page_heading','Список заявок')

@section('section')


    <div class="row">
        <div class="col-sm-12">
            <form method="GET" >
            @section ('filter_panel_title','Фильтр')
            @section ('filter_panel_body')


                    <div class="row" >
                        <div class="col-sm-1">
                            <input class="form-control" placeholder="Номер заявки" name="order_id" value="{{$filter['order_id']}}">
                        </div>
                        <div class="col-sm-1">
                            <input class="form-control datepicker_input" placeholder="Дата заявки с" name="order_date_from" value="{{$filter['order_date_from']}}">
                        </div>
                        <div class="col-sm-1">
                            <input class="form-control datepicker_input" placeholder="Дата заявки по" name="order_date_to" value="{{$filter['order_date_to']}}">
                        </div>
                        <div class="col-sm-1">
                            <input class="form-control datepicker_input" placeholder="Поездка c" name="tour_date_from" value="{{$filter['tour_date_from']}}">
                        </div>
                        <div class="col-sm-1">
                            <input class="form-control datepicker_input" placeholder="Поездка по" name="tour_date_to" value="{{$filter['tour_date_to']}}">
                        </div>
                        <div class="col-sm-2">

                            <div class="form-group input-group ">
                                <input class="form-control"  type="hidden" name="country_id"  value="{{$filter['country_id']}}"  />
                                <input class="form-control filter_country_autocomplete" type="text"  placeholder="Введите страну" value="{{$filter['country_name']}}"  />
                            </div>

                        </div>
                        <div class="col-sm-2">
                            <select class="form-control" name="tourop_id">
                                <option value="0">Выберите туроператора</option>
                                @foreach( $tourops as $tourop )
                                    <option value="{{$tourop->id}}" {{ $filter['tourop_id'] == $tourop->id ? 'selected' : ''  }}>{{$tourop->short_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <select class="form-control" name="user_id">
                                <option value="0">Выберите менеджера</option>
                                @foreach( $users as $user )
                                    <option value="{{$user->id}}" {{ $filter['user_id'] == $user->id ? 'selected' : ''  }}>{{$user->fullname()}}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>
                   <!-- <div class="row">
                        <div class="col-sm-4">
                            <select class="form-control" name="client_payment_status">
                                <option value="0" >Выберите статус оплат от туриста</option>
                                <option value="1" >Нет оплат туриста</option>
                                <option value="2" >Предоплачено туристом</option>
                                <option value="3" >Полностью оплачена туристом</option>
                            </select>
                        </div>

                        <div class="col-sm-4">
                            <select class="form-control" name="tourop_payment_status">
                                <option value="0" >Выберите статус оплат туроператору</option>
                                <option value="1" >Нет оплат туроператору</option>
                                <option value="2" >Предоплачено туроператору</option>
                                <option value="3" >Полностью оплачена туроператору</option>
                            </select>
                        </div>
                    </div>-->


            @endsection

            @section ('filter_panel_footer')

                <button type="submit" class="btn btn-default" >Поиск</button>

                <button type="button" class="btn btn-default filter_reset" >Сброс</button>

            @endsection

            @include('widgets.panel', array('header'=>true ,'footer' => true , 'as'=>'filter'))

            </form>

            <a href="{{ url ('/orders/edit' ) }}" class="btn btn-primary" >
                <i class="fa fa-plus" ></i> Добавить завку
            </a>

            <br/><br/>

            @if( count( $orders ) >0 )

                <table class="table table-bordered">
                    <thead>
                        <tr >
                            <td></td>
                            <td>№/дата создания</td>
                            <td>Страна</td>
                            <td>Туроператор</td>
                            <td>Менеджер</td>
                            <td>Стоимость</td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach( $orders as $row )
                        <tr >
                            <td><a href="{{ url ('/orders/edit/' . $row->id ) }}"  ><i class="fa fa-edit fa-fw "></i></a></td>
                            <td><b>{{ $row->id }}</b> / {{ \Carbon\Carbon::parse( $row->created_at )->format( 'd.m.Y H:i' )}}</td>
                            <td>{{ $row->accomodations->count() > 0 ?$row->accomodations->first()->country->name : '---' }}</td>
                            <td>{{ $row->tourop->short_name }}</td>
                            <td>{{ $row->user->fullname() }}</td>
                            <td>{{ $row->full_price() }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <h3>Ничего не найдено</h3>
            @endif
        </div>
    </div>


@endsection