
<div class="insurance_row"   >

    <div class="col-sm-5">

        <select class="form-control" name="insurance[type][]">
            <option value="0" >Не выбрано</option>
            <option value="{{\App\Http\Models\Insurance::MEDICAL}}" {{ $insurance->type == \App\Http\Models\Insurance::MEDICAL ? 'selected' : ''  }}>Медицинская</option>
            <option value="{{\App\Http\Models\Insurance::UNABLE_TRAVEL}}" {{ $insurance->type == \App\Http\Models\Insurance::UNABLE_TRAVEL ? 'selected' : ''  }}>От невыезда</option>
        </select>

    </div>
    <div class="col-sm-6">

        <div class="row" >
            <div class="col-lg-6" >

                <input type="number" class="form-control" name="insurance[cost][]" value="{{  $insurance->cost }}">

            </div>

            <div class="col-lg-6" >

                <input type="number" class="form-control" name="insurance[count][]" value="{{  $insurance->count }}">

            </div>
        </div>
    </div>

    <div class="col-sm-1" align="center">
        <button type="button" class="btn btn-danger btn-circle remove_insurance"><i class="fa fa-remove"></i></button>
    </div>

    <div class="clearfix"></div>

</div>