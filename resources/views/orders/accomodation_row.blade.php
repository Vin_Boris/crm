
<div class="accomodation_row"   >
    <div class="col-sm-1">
        <div class="form-group input-group ">
            <input class="form-control"  type="hidden" name="accomodations[country][]"  value="{{ $accomodation->country_id }}" />
            <input class="form-control country_autocomplete" type="text"   value="{{ $accomodation->country? $accomodation->country->name : '' }}" placeholder="Введите страну"/>
            <span class="input-group-btn" >
                <button type="button" class="btn btn-default "><i class="fa fa-check"></i></button>
            </span>

        </div>

    </div>

    <div class="col-sm-2">
        <div class="form-group input-group ">
            <input class="form-control"  type="hidden" name="accomodations[town][]"  value="{{ $accomodation->town_id }}" />
            <input class="form-control town_autocomplete" type="text"   value="{{ $accomodation->town? $accomodation->town->name : '' }}" placeholder="Введите город" {{ $accomodation->country_id > 0 ? '' : 'disabled' }}/>
            <span class="input-group-btn" >
                    <button type="button" class="btn btn-default "><i class="fa fa-check"></i></button>
            </span>
        </div>

    </div>

    <div class="col-sm-2">

        <div class="form-group input-group ">
            <input class="form-control"  type="hidden" name="accomodations[hotel][]"  value="{{ $accomodation->hotel_id }}" />
            <input class="form-control hotel_autocomplete" type="text"   value="{{ $accomodation->hotel? $accomodation->hotel->name : '' }}" placeholder="Введите отель" {{ $accomodation->town_id > 0 ? '' : 'disabled' }} />
            <span class="input-group-btn" >
                    <button type="button" class="btn btn-default "><i class="fa fa-check"></i></button>
            </span>
        </div>

    </div>

    <div class="col-sm-1">

        <div class="form-group input-group ">
            <input class="form-control"  type="hidden" name="accomodations[acc][]"  value="{{ $accomodation->accomodation_id }}" />
            <input class="form-control acc_autocomplete" type="text"   value="{{ $accomodation->accomodation? $accomodation->accomodation->name : '' }}" placeholder="Введите размещение" />
            <span class="input-group-btn" >
                    <button type="button" class="btn btn-default "><i class="fa fa-check"></i></button>
            </span>
        </div>

    </div>

    <div class="col-sm-1">

        <div class="form-group input-group ">
            <input class="form-control"  type="hidden" name="accomodations[food][]"  value="{{ $accomodation->food_id }}" />
            <input class="form-control food_autocomplete" type="text"   value="{{ $accomodation->food? $accomodation->food->name : '' }}" placeholder="Введите размещение" />
            <span class="input-group-btn" >
                    <button type="button" class="btn btn-default "><i class="fa fa-check"></i></button>
            </span>
        </div>

    </div>

    <div class="col-sm-2">

        <div class="row" >
            <div class="col-lg-6" >
                 <input type="text" class="form-control date_from" name="accomodations[date_from][]" value="{{  \Carbon\Carbon::parse( $accomodation->date_from )->format( 'd.m.Y' ) }}" >
            </div>

            <div class="col-lg-6" >
                    <input type="text" class="form-control date_to"  name="accomodations[date_to][]" value="{{  \Carbon\Carbon::parse( $accomodation->date_to )->format( 'd.m.Y' ) }} ">
            </div>
        </div>
    </div>

    <div class="col-lg-1" >

        <input type="number" class="form-control" name="accomodations[cost][]" value="{{  $accomodation->cost }}">

    </div>

    <div class="col-lg-1" >

        <input type="number" class="form-control" name="accomodations[count][]" value="{{  $accomodation->count }}">

    </div>


    <div class="col-sm-1" align="center">
        <button type="button" class="btn btn-danger btn-circle remove_accomodation"><i class="fa fa-remove"></i></button>
    </div>

    <div class="clearfix"></div>

</div>