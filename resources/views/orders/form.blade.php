@extends('layouts.dashboard')
@section('page_heading', $order->exists ? 'Заявка №' . $order->id : 'Создание заявки' )
@section('section')

    <div class="row">

        <form method="POST" action="/orders/save/{{ $order->exists ? $order->id : 0 }}" id="order_form">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="col-sm-12">

        <div class="alert hidden alert-danger" id="save_message"></div>
        <div class="clearfix"></div>

        <ul class="nav nav-tabs">
            <li role="tab" class="active"><a data-toggle="tab" href="#order">Заявка</a></li>
            <li role="tab"><a data-toggle="tab" href="#payments">Финансовая информация</a></li>

            @if( $order->exists )
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Документы
                    <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="{{ url( 'orders/contract/'. $order->id ) }}" target="_blank" >Договор</a></li>
                    <li><a href="{{ url( 'orders/contract-app-1/'. $order->id ) }}" target="_blank" >Приложение 1</a></li>
                    <li><a href="{{ url( 'orders/contract-app-2/'. $order->id ) }}" target="_blank" >Приложение 2</a></li>
                </ul>
            </li>
            @endif
        </ul>

        <div class="tab-content">
            <div id="order" class="tab-pane fade in active ">
                <br/><br/>

            @if( $order->exists )

            <div class="well">
                <div class="col-lg-6">
                    Пользователь : <b>{{ $order->user->fullname() }}</b>
                </div>
                <div class="col-lg-6">
                    Дата заявки  : {{ \Carbon\Carbon::parse( $order->created_at )->format( 'd.m.Y H:i' ) }}
                </div>
                <div class="clearfix"></div>
            </div>

            @endif


            <div class="row"   >
                <div class="col-sm-12">
                    <label>Турист для договора</label>
                </div>
                <div class="clearfix"></div>
                <div class="col-lg-11" >
                    <input type="hidden" name="tourist_for_contract" value="{{  $order->client_for_contract ? $order->client_for_contract->id : 0 }}" >
                    <div id="tourist_for_contract" class="alert {{  $order->client_for_contract ? 'alert-info' : 'alert-danger' }}"  data-id="{{  $order->client_for_contract ? $order->client_for_contract->id : 0 }}">
                        @if(  $order->client_for_contract  )
                            {{ $order->client_for_contract->fullname() }}
                            {{  \Carbon\Carbon::parse( $order->client_for_contract->birthday )->format( 'd.m.Y' ) }}
                        @else
                                Добавьте туриста для договора!

                        @endif
                    </div>
                    <div class="clearfix"></div>

                </div>

                <div class="col-lg-1">

                     <button type="button" class="btn btn-primary btn-circle" id="add_to_tourist_to_contract" ><i class="fa fa-list"></i></button>

                </div>
            </div>

            <div class="clearfix"></div>

            @section ('tourists_panel_title','Туристы')
            @section ('tourists_panel_body')

                <div class="row" >

                    <div class="col-sm-8">
                        <label>Турист</label>
                    </div>

                    <div class="col-sm-3">
                        <label>Период пребывания</label>
                    </div>
                    <div class="col-sm-1" align="center">
                        <label>Удалить</label>
                    </div>

                    <div class="clearfix"></div>

                    <div id="tourists_row_container" >
                        @if( $tourists->count()  >0)
                            @foreach( $tourists as $tourist )
                                @include('orders.tourist_row', array('tourist'=> $tourist  ))
                            @endforeach
                        @else

                            <div class="col-sm-12" id="tourists_row_empty_message" >
                                <div class="alert alert-warning " >
                                    Добавьте туристов!
                                </div>
                            </div>

                        @endif
                    </div>
                </div>

            @endsection

            @section ('tourists_panel_footer')
                <div align="right">
                    <input type="button" class="btn btn-success " id="add_client" value="Добавить туриста" />
                </div>
            @endsection

            @include('widgets.panel', array('header'=>true , 'footer' => true, 'as'=>'tourists'))

            <div id="accomodation_row_template" class="hidden">
                @include('orders.accomodation_row', array('accomodation'=> new \App\Http\Models\AccomodationOrder()  ))
            </div>

            @section ('acc_panel_title','Размещения')
            @section ('acc_panel_body')
                <div class="form-group" id="accomodations">
                    <div class="row" >

                        <div class="col-sm-1">
                            <label>Страна</label>
                        </div>

                        <div class="col-sm-2">
                            <label>Город</label>
                        </div>
                        <div class="col-sm-2" align="center">
                            <label>Отель</label>
                        </div>

                        <div class="col-sm-1" align="center">
                            <label>Размещение</label>
                        </div>

                        <div class="col-sm-1" align="center">
                            <label>Питание</label>
                        </div>

                        <div class="col-sm-2">
                            <label>Период пребывания</label>
                        </div>

                        <div class="col-sm-1">
                            <label>Цена</label>
                        </div>

                        <div class="col-sm-1">
                            <label>Количество</label>
                        </div>

                        <div class="col-sm-1" align="center">
                            <label>Удалить</label>
                        </div>

                        <div class="clearfix"></div>

                        <div id="accomodation_row_container" >
                            @if( $accomodations->count()  >0)
                                @foreach( $accomodations as $accomodation )
                                    @include('orders.accomodation_row', array('accomodation'=> $accomodation  ))
                                @endforeach
                            @else

                                <div class="col-sm-12" id="accomodation_row_empty_message">
                                    <div class="alert alert-warning " >
                                        Добавьте размещения!
                                    </div>
                                </div>

                            @endif
                        </div>

                    </div>

                </div>
            @endsection

            @section ('acc_panel_footer')
                <div align="right">
                    <button type="button" class="btn btn-success " id="add_accomodation">Добавить размещение</button>
                </div>
            @endsection

            @include('widgets.panel', array('header'=>true , 'footer' => true, 'as'=>'acc'))


            <div id="transfer_row_template" class="hidden">
                @include('orders.transfer_row', array('transfer'=> new \App\Http\Models\Transfer()  ))
            </div>

            @section ('transfer_panel_title','Трансферы')
            @section ('transfer_panel_body')
                <div class="form-group" id="transfers">
                    <div class="row" >

                        <div class="col-sm-4">
                            <label>Город</label>
                        </div>

                        <div class="col-sm-3">
                            <label>Назначение</label>
                        </div>

                        <div class="col-sm-2">
                            <label>Тип</label>
                        </div>

                        <div class="col-sm-2">
                            <label>Количество</label>
                        </div>

                        <div class="col-sm-1" align="center">
                            <label>Удалить</label>
                        </div>

                        <div class="clearfix"></div>

                        <div id="transfer_row_container" >
                            @if( $transfers->count()  >0)
                                @foreach( $transfers as $transfer )
                                    @include('orders.transfer_row', array('transfer'=> $transfer  ))
                                @endforeach
                            @else

                                <div class="col-sm-12" id="transfer_row_empty_message">
                                    <div class="alert alert-warning "  >
                                        Добавьте трансфер!
                                    </div>
                                </div>

                            @endif
                        </div>

                    </div>

                </div>
            @endsection

            @section ('transfer_panel_footer')
                <div align="right">
                    <button type="button" class="btn btn-success " id="add_transfer">Добавить трансфер</button>
                </div>
            @endsection

            @include('widgets.panel', array('header'=>true , 'footer' => true, 'as'=>'transfer'))





            <div id="ticket_row_template" class="hidden">
                @include('orders.ticket_row', array('ticket'=> new \App\Http\Models\Ticket()  ))
            </div>

            @section ('tickets_panel_title','Перевозки')
            @section ('tickets_panel_body')
                <div class="form-group" id="tickets">
                    <div class="row" >

                        <div class="col-sm-3">
                            <label>Город отправления</label>
                        </div>

                        <div class="col-sm-3">
                            <label>Город прибытия</label>
                        </div>

                        <div class="col-sm-2">
                            <label>Назначение</label>
                        </div>

                        <div class="col-sm-1">
                            <label>Дата</label>
                        </div>

                        <div class="col-sm-1">
                            <label>Цена</label>
                        </div>

                        <div class="col-sm-1">
                            <label>Количество</label>
                        </div>

                        <div class="col-sm-1" align="center">
                            <label>Удалить</label>
                        </div>

                        <div class="clearfix"></div>

                        <div id="tickets_row_container" >
                            @if( $tickets->count()  >0)
                                @foreach( $tickets as $ticket )
                                    @include('orders.ticket_row', array('ticket'=> $ticket  ))
                                @endforeach
                            @else

                                <div class="col-sm-12" id="ticket_row_empty_message">
                                    <div class="alert alert-warning "  >
                                        Добавьте перевозку!
                                    </div>
                                </div>

                            @endif
                        </div>

                    </div>

                </div>
            @endsection

            @section ('tickets_panel_footer')
                <div align="right">
                    <button type="button" class="btn btn-success " id="add_ticket">Добавить перевозку</button>
                </div>
            @endsection

            @include('widgets.panel', array('header'=>true , 'footer' => true, 'as'=>'tickets'))


            <div id="insurance_row_template" class="hidden">
                @include('orders.insurance_row', array( 'insurance' => new \App\Http\Models\Insurance()  ))
            </div>

            @section ('insurance_panel_title','Страхование')
            @section ('insurance_panel_body')
                <div class="form-group">
                    <div class="row" >

                        <div class="col-sm-5">
                            <label>Тип</label>
                        </div>

                        <div class="col-sm-3">
                            <label>Цена</label>
                        </div>

                        <div class="col-sm-3">
                            <label>Количество</label>
                        </div>

                        <div class="col-sm-1" align="center">
                            <label>Удалить</label>
                        </div>

                        <div class="clearfix"></div>

                        <div id="insurance_row_container" >
                            @if( $insurances->count()  >0)
                                @foreach( $insurances as $insurance )
                                    @include('orders.insurance_row', array('insurance'=> $insurance  ))
                                @endforeach
                            @else

                                <div class="col-sm-12" id="insurance_row_empty_message">
                                    <div class="alert alert-warning "  >
                                        Добавьте страховку!
                                    </div>
                                </div>

                            @endif
                        </div>

                    </div>

                </div>
            @endsection

            @section ('insurance_panel_footer')
                <div align="right">
                    <button type="button" class="btn btn-success " id="add_insurance" >Добавить страховку</button>
                </div>
            @endsection

            @include('widgets.panel', array('header'=>true , 'footer' => true, 'as'=>'insurance'))


            <div id="visas_row_template" class="hidden">
                @include('orders.visas_row', array('visa'=> new \App\Http\Models\Visa()  ))
            </div>

            @section ('visas_panel_title','Визы')
            @section ('visas_panel_body')
                <div class="form-group">
                    <div class="row" >

                        <div class="col-sm-3">
                            <label>Страна</label>
                        </div>

                        <div class="col-sm-5">
                            <label>Комментарии</label>
                        </div>

                        <div class="form-group">
                            <label>Туроператор</label>
                            <select class="form-control" name="tourop_id">
                                <option value="0">Выберите туроператора</option>
                                @foreach( $tourops as $tourop )
                                    <option value="{{$tourop->id}}" {{ $order->tourop_id == $tourop->id ? 'selected' : ''  }} >{{$tourop->short_name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-sm-2">
                            <label>Цена</label>
                        </div>

                        <div class="col-sm-1">
                            <label>Количество</label>
                        </div>

                        <div class="col-sm-1" align="center">
                            <label>Удалить</label>
                        </div>

                        <div class="clearfix"></div>

                        <div id="visas_row_container" >
                            @if( $visas->count()  >0)
                                @foreach( $visas as $visa )
                                    @include('orders.visas_row', array('visa'=> $visa  ))
                                @endforeach
                            @else

                                <div class="col-sm-12" id="visas_row_empty_message">
                                    <div class="alert alert-warning "  >
                                        Добавьте визы!
                                    </div>
                                </div>

                            @endif
                        </div>

                    </div>

                </div>
            @endsection

            @section ('visas_panel_footer')
                <div align="right">
                    <button type="button" class="btn btn-success " id="add_visa">Добавить визу</button>
                </div>
            @endsection

            @include('widgets.panel', array('header'=>true , 'footer' => true, 'as'=>'visas'))

            <div id="services_row_template" class="hidden">
                @include('orders.services_row', array('service'=> new \App\Http\Models\OrderService()  ))
            </div>

            @section ('dop_panel_title','Дополнительные услуги')
            @section ('dop_panel_body')
                <div class="form-group">
                    <div class="row" >

                        <div class="col-sm-8">
                            <label>Название</label>
                        </div>

                        <div class="col-sm-2">
                            <label>Цена</label>
                        </div>

                        <div class="col-sm-1">
                            <label>Количество</label>
                        </div>

                        <div class="col-sm-1" align="center">
                            <label>Удалить</label>
                        </div>

                        <div class="clearfix"></div>

                        <div id="services_row_container" >
                            @if( $services->count()  >0)
                                @foreach( $services as $service )
                                    @include('orders.services_row', array('service'=> $service  ))
                                @endforeach
                            @else

                                <div class="col-sm-12" id="services_row_empty_message">
                                    <div class="alert alert-warning "  >
                                        Добавьте доп услуги!
                                    </div>
                                </div>

                            @endif
                        </div>

                    </div>

                </div>
            @endsection

            @section ('dop_panel_footer')
                <div align="right">
                    <button type="button" class="btn btn-success " id="add_order_service">Добавить услугу</button>
                </div>
            @endsection

            @include('widgets.panel', array('header'=>true , 'footer' => true, 'as'=>'dop'))


            <div class="form-group">
                <label>Комментарии</label>
                <textarea class="form-control" rows="3" name="description">{{ $order->description }}</textarea>
            </div>

            <div class="form-group">
                <label>Туроператор</label>
                <select class="form-control" name="tourop_id">
                    <option value="0">Выберите туроператора</option>
                    @foreach( $tourops as $tourop )
                        <option value="{{$tourop->id}}" {{ $order->tourop_id == $tourop->id ? 'selected' : ''  }} >{{$tourop->short_name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label>Стоимость в у.е.</label>
                <input class="form-control"  name="price" value="{{ $order->cost }}">
            </div>

            <div class="form-group">
                <label>Валюта</label>
                <select class="form-control"  name="currency" >
                    <option value="{{ \App\Http\Models\Currency::RUB }}" {{ $order->currency == \App\Http\Models\Currency::RUB ? 'selected' : '' }} >RUB</option>
                    <option value="{{ \App\Http\Models\Currency::USD }}" {{ $order->currency == \App\Http\Models\Currency::USD ? 'selected' : '' }}>USD</option>
                    <option value="{{ \App\Http\Models\Currency::EUR }}" {{ $order->currency == \App\Http\Models\Currency::EUR ? 'selected' : '' }}>EUR</option>
                </select>
            </div>

            <div class="form-group">
                <label>Курс у.е.</label>
                <input class="form-control"  name="currency_rate" value="{{ $order->currency_rate }}">
            </div>

            <div class="form-group">
                <label>Стоимость в руб.</label>
                <input class="form-control"  name="price_rub" value="{{ $order->cost_rub }}">
            </div>


            <div class="form-group">
                <label>Скидка %</label>
                <input class="form-control"  name="discount"   value="{{ $order->discount }}" >
            </div>

            <div class="form-group">
                <label>Конечная стоимость</label>
                <input class="form-control"  name="full_price"  value="{{ $order->cost - $order->discount_sum }}">
            </div>

            <div class="form-group">
                <label>Полная оплата турпродукта производится в срок до</label>
                <input class="form-control datepicker_input"  name="full_payment_date"  value="{{ $order->full_payment_date ? \Carbon\Carbon::parse( $order->full_payment_date )->format('d.m.Y') : '' }}">
            </div>



            </div>

            <div id="payments" class="tab-pane fade in ">
                <br/><br/>

                <div id="client_payments_template" class="hidden">
                    @include('orders.client_payment_row', array('payment'=> new \App\Http\Models\ClientPayment()  ))
                </div>


                @section ('tourist_payments_panel_title','Оплаты туриста')
                @section ('tourist_payments_panel_body')
                    <div class="form-group">
                        <div class="row" >

                            <div class="col-sm-1">
                                <label>+\-</label>
                            </div>

                            <div class="col-sm-3">
                                <label>Сумма</label>
                            </div>

                            <div class="col-sm-3">
                                <label>Тип оплаты</label>
                            </div>

                            <div class="col-sm-2">
                                <label>Дата</label>
                            </div>

                            <div class="col-sm-2">
                                <label>Кассовый ордер</label>
                            </div>

                            <div class="col-sm-1" align="center">
                                <label>Удалить</label>
                            </div>

                            <div class="clearfix"></div>

                            <div id="client_payments_row_container" >
                                @if( $client_payments->count()  >0)
                                    @foreach( $client_payments as $client_payment )
                                        @include('orders.client_payment_row', array('payment'=> $client_payment  ))
                                    @endforeach
                                @else

                                    <div class="col-sm-12" id="client_payments_row_empty_message">
                                        <div class="alert alert-warning "  >
                                            Добавьте оплату туриста!
                                        </div>
                                    </div>

                                @endif
                            </div>

                        </div>

                    </div>
                @endsection

                @section ('tourist_payments_panel_footer')

                    <div class="row" >
                        <div class="col-lg-6">
                            <b>Итого: <span id="client_payment_amount">0</span></b>
                        </div>
                        <div align="right" class="col-lg-6" >
                            <button type="button" class="btn btn-success " id="add_client_payment" >Добавить оплату</button>
                        </div>
                    </div>

                @endsection

                @include('widgets.panel', array('header'=>true , 'footer' => true, 'as'=>'tourist_payments'))


                <div id="confirmations_template" class="hidden">
                    @include('orders.confirmations_row', array('confirmation'=> new \App\Http\Models\OrderConfirmation()  ))
                </div>


                @section ('confirmations_panel_title','Подтверждения')
                @section ('confirmations_panel_body')
                    <div class="form-group">
                        <div class="row" >

                            <div class="col-sm-2">
                                <label>Дата</label>
                            </div>

                            <div class="col-sm-2">
                                <label>Номер</label>
                            </div>

                            <div class="col-sm-3">
                                <label>Сумма к оплате</label>
                            </div>

                            <div class="col-sm-2">
                                <label>Валюта</label>
                            </div>

                            <div class="col-sm-2">
                                <label>Файл</label>
                            </div>

                            <div class="col-sm-1" align="center">
                                <label>Удалить</label>
                            </div>

                            <div class="clearfix"></div>

                            <div id="confirmations_row_container" >
                                @if( $confirmations->count()  >0)
                                    @foreach( $confirmations as $confirmation )
                                        @include('orders.confirmations_row', array('confirmation'=> $confirmation  ))
                                    @endforeach
                                @else

                                    <div class="col-sm-12" id="confirmations_row_empty_message">
                                        <div class="alert alert-warning "  >
                                            Добавьте подтверждение!
                                        </div>
                                    </div>

                                @endif
                            </div>

                        </div>

                    </div>
                @endsection

                @section ('confirmations_panel_footer')

                    <div class="row" >
                        <div class="col-lg-6">
                            <b>Итого: <span id="confirmations_payment_amount">0</span> </b>
                        </div>
                        <div align="right" class="col-lg-6" >
                            <button type="button" class="btn btn-success " id="add_confirmation" >Добавить подтверждение</button>
                        </div>
                    </div>

                @endsection

                @include('widgets.panel', array('header'=>true , 'footer' => true, 'as'=>'confirmations'))

                <hr/>

                <hr/>

                <div id="tourop_payments_template" class="hidden">
                    @include('orders.tourop_payment_row', array('payment'=> new \App\Http\Models\TouropPayment()  ))
                </div>

                @section ('tourop_payments_panel_title','Оплаты туроператору')
                @section ('tourop_payments_panel_body')
                    <div class="form-group">
                        <div class="row" >

                            <div class="col-sm-1">
                                <label>+\-</label>
                            </div>

                            <div class="col-sm-2">
                                <label>Сумма</label>
                            </div>

                            <div class="col-sm-2">
                                <label>Курс у.е.</label>
                            </div>

                            <div class="col-sm-2">
                                <label>Сумма в у.е.</label>
                            </div>

                            <div class="col-sm-2">
                                <label>Тип оплаты</label>
                            </div>

                            <div class="col-sm-2">
                                <label>Дата</label>
                            </div>

                            <div class="col-sm-1" align="center">
                                <label>Удалить</label>
                            </div>

                            <div class="clearfix"></div>

                            <div id="tourop_payments_row_container" >
                                @if( $tourop_payments->count()  >0)
                                    @foreach( $tourop_payments as $tourop_payment )
                                        @include('orders.tourop_payment_row', array('payment'=> $tourop_payment  ))
                                    @endforeach
                                @else

                                    <div class="col-sm-12" id="tourop_payments_row_empty_message">
                                        <div class="alert alert-warning "  >
                                            Добавьте оплату туроператора!
                                        </div>
                                    </div>

                                @endif
                            </div>

                        </div>

                    </div>
                @endsection

                @section ('tourop_payments_panel_footer')
                    <div class="row" >
                        <div class="col-lg-6">
                            <b>Итого: <span id="tourop_payment_amount">0</span> </b>
                        </div>
                        <div align="right" class="col-lg-6" >
                            <button type="button" class="btn btn-success " id="add_tourop_payment" >Добавить оплату</button>
                        </div>
                    </div>
                @endsection

                @include('widgets.panel', array('header'=>true , 'footer' => true, 'as'=>'tourop_payments'))


            </div>

            <button type="submit" class="btn btn-default">Сохранить</button>

        </div>
    </div>
        </form>
    </div>

@endsection