@extends('layouts.dashboard')

@section('page_heading','Форма юр. лица')

@section('section')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="col-sm-12">
            <form method="POST" action="{{ url ('/legal_entities/save' . ( $legal_entity->id > 0 ? '/' . $legal_entity->id : '' ) ) }}">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                 <div class="form-group">
                    <label>Название</label>
                    <input class="form-control" name="name" value="{{ $legal_entity->name }}" >
                </div>

                <div class="form-group">
                    <label>Орг правовая форма</label>

                    <select class="form-control" name="legal_from" >
                        <option value="0" >не выбрано </option>
                        @foreach( $legal_forms as $legal_form )
                            <option value="{{$legal_form->id}}" {{ ( $legal_form->id == $legal_entity->orgsform_id ? 'selected' : '' )  }} >{{ $legal_form->name  }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label>Адрес</label>
                    <input class="form-control" name="address" value="{{ $legal_entity->address }}" >
                </div>

                <div class="form-group">
                    <label>Телефон</label>
                    <input class="form-control" name="phone" value="{{ $legal_entity->phone }}" >
                </div>

                <div class="form-group">
                    <label>ФИН директора</label>
                    <input class="form-control" name="director_fio" value="{{ $legal_entity->director_fio }}" >
                </div>

                <div class="form-group">
                    <label>ОГРН/ОГРНИП</label>
                    <input class="form-control" name="ogrn" value="{{ $legal_entity->ogrn }}" >
                </div>

                <div class="form-group">
                    <label>Город</label>
                    <input class="form-control" name="town" value="{{ $legal_entity->town }}" >
                </div>

                <div class="form-group">
                    <label>ОКПО</label>
                    <input class="form-control" name="okpo" value="{{ $legal_entity->okpo }}" >
                </div>

                <div class="form-group">
                    <label>ОКАТО</label>
                    <input class="form-control" name="okato" value="{{ $legal_entity->okato }}" >
                </div>

                <button type="submit" class="btn btn-default">Сохранить</button>

            </form>

            <div>

                <h2> Список счетов </h2>

                <a href="{{ url ('/legal_entities/accounts/edit/' . $legal_entity->id ) }}" class="btn btn-primary" target="_blank">
                    <i class="fa fa-plus" ></i> Добавить счет
                </a>

                <br/><br/>

                @if( count( $accounts ) >0 )

                    <table class="table table-bordered">
                        <thead>
                            <th></th>
                            <th>Р/с</th>
                            <th>БИК</th>
                            <th>Банк</th>
                            <th>К/с</th>
                        </thead>
                        <tbody>
                        @foreach( $accounts as $row )
                            <tr >
                                <td><a href="{{ url ('/legal_entities/accounts/edit/' . $row->id ) }}" target="_blank" >
                                        <i class="fa fa-edit fa-fw "></i>
                                    </a>
                                </td>
                                <td>{{ $row->rs }}</td>
                                <td>{{ $row->bik }}</td>
                                <td>{{ $row->bank }}</td>
                                <td>{{ $row->ks }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <h3>Ничего не найдено</h3>
                @endif
            </div>

            </div>


        </div>
    </div>

@endsection