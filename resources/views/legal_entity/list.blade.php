@extends('layouts.dashboard')

@section('page_heading','Список Юр.лиц')

@section('section')


    <div class="row">
        <div class="col-sm-12">

            <a href="{{ url ('/legal_entities/edit' ) }}" class="btn btn-primary" >
                <i class="fa fa-plus" ></i> Добавить Юр. лицо
            </a>

            <br/><br/>

            @if( count( $legal_entities ) >0 )

            <table class="table table-bordered">
                <tbody>
                @foreach( $legal_entities as $row )
                <tr >
                    <td><a href="{{ url ('/legal_entities/edit/' . $row->id ) }}"  >
                            <i class="fa fa-edit fa-fw "></i>
                        </a>
                    </td>
                    <td>{{ $row->name }}</td>
                    <td>{{ $row->phone }}</td>
                    <td>{{ $row->email }}</td>
                    <td>{{ $row->legal_entity ? $row->legal_entity->name : 'Не выбрано' }}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
            @else
                <h3>Ничего не найдено</h3>
            @endif
        </div>
    </div>

@endsection