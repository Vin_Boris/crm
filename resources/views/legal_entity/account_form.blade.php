@extends('layouts.dashboard')

@section('page_heading','Форма счета' )

@section('section')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="row">
        <div class="col-sm-12">
            <form method="POST" action="{{ url ('/legal_entities/accounts/save/' . $entity_account->jurname_id  . ( $entity_account->id > 0 ? '/' . $entity_account->id : '' ) ) }}">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group">
                    <label>Р/с</label>
                    <input class="form-control" name="rs" value="{{ $entity_account->rs }}" >
                </div>

                <div class="form-group">
                    <label>БИК</label>
                    <input class="form-control" name="bik" value="{{ $entity_account->bik }}" >
                </div>

                <div class="form-group">
                    <label>Банк</label>
                    <input class="form-control" name="bank" value="{{ $entity_account->bank }}" >
                </div>

                <div class="form-group">
                    <label>К/с</label>
                    <input class="form-control" name="ks" value="{{ $entity_account->ks }}" >
                </div>

                <button type="submit" class="btn btn-default">Сохранить</button>

            </form>

        </div>

    </div>

@endsection