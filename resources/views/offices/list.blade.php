@extends('layouts.dashboard')

@section('page_heading','Список офисов')

@section('section')



    <div class="row">
        <div class="col-sm-12">

            <a href="{{ url ('/offices/edit' ) }}" class="btn btn-primary" >
                <i class="fa fa-plus" ></i> Добавить офис
            </a>

            <br/><br/>

            @if( count( $data ) >0 )

            <table class="table table-bordered">
                <tbody>
                @foreach( $data as $row )
                <tr >
                    <td><a href="{{ url ('/offices/edit' . $row->id ) }}"><i class="fa fa-edit fa-fw "></i></a></td>
                    <td>{{ $row->name }}</td>
                    <td>{{ $row->legal_entity->name }}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
            @else
                <h3>Ничего не найдено</h3>
            @endif
        </div>
    </div>

@endsection