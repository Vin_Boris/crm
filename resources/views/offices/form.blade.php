@extends('layouts.dashboard')

@section('page_heading','Форма офиса')

@section('section')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="col-sm-12">
            <form method="POST" action="{{ url ('/offices/save' . ( $office->id > 0 ? '/' . $office->id : '' ) ) }}">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                 <div class="form-group">
                    <label>Название</label>
                    <input class="form-control" name="name" value="{{ $office->name }}" >
                </div>

                <div class="form-group">
                    <label>Юр лицо</label>

                    <select class="form-control" name="legal_entity" >
                        <option value="0" >не выбрано </option>
                        @foreach( $legal_entities as $legal_entity )
                            <option value="{{$legal_entity->id}}" {{ ( $legal_entity->id == $office->jurname_id ? 'selected' : '' )  }} >{{ $legal_entity->name  }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label>Адрес</label>
                    <input class="form-control" name="address" value="{{ $office->address }}" >
                </div>

                <div class="form-group">
                    <label>Email</label>
                    <input class="form-control" name="email" value="{{ $office->email }}" >
                </div>

                <div class="form-group">
                    <label>Телефон</label>
                    <input class="form-control" name="phone" value="{{ $office->phone }}">
                </div>

                <button type="submit" class="btn btn-default">Сохранить</button>

            </form>
        </div>
    </div>

@endsection