@extends('layouts.plane')

@section('body')
    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url ('/orders') }}">CRM</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">

                <!-- /.dropdown -->

                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  {{ \Auth::user()->last_name }} {{ \Auth::user()->first_name }} {{ \Auth::user()->middle_name }} <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="{{ url ('/users/edit/' . \Auth::user()->id) }}"><i class="fa fa-gear fa-fw"></i> Редактировать</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="{{ url ('logout') }}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">

                        <li >
                            <a href="#"><i class="fa fa-list" aria-hidden="true"></i> Заявки<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li >
                                    <a href="/orders">Список заявок</a>
                                </li>
                                <li {{ (Request::is('*orders') ? 'class="active"' : '') }}>
                                    <a href="{{ url ('orders/edit') }}">Создать заявку</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li >
                            <a href={{ url ('clients') }}><i class="fa fa-list" aria-hidden="true"></i> Клиенты</a>
                        </li>
                        <li >
                            <a href="#"><i class="fa fa-users" aria-hidden="true"></i> Офисы и пользователи<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li {{ (Request::is('*users') ? 'class="active"' : '') }}>
                                    <a href="{{ url ('users') }}">Пользователи</a>
                                </li>


                                <li {{ (Request::is('*offices') ? 'class="active"' : '') }}>
                                    <a href="{{ url ('offices') }}">Офисы</a>
                                </li>

                                <li {{ (Request::is('*legal_forms') ? 'class="active"' : '') }}>
                                    <a href="{{ url ('legal_forms') }}">Орг-правовые формы</a>
                                </li>

                                <li {{ (Request::is('*legal_entities') ? 'class="active"' : '') }}>
                                    <a href="{{ url ('legal_entities') }}">Юридические лица</a>
                                </li>

                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li {{ (Request::is('*users') ? 'class="active"' : '') }} >
                            <a href="{{ url ('tour_operators') }}"><i class="fa fa-plane" aria-hidden="true"></i> Туроператоры </a>
                            <!-- /.nav-second-level -->
                        </li>



                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">@yield('page_heading')</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                @yield('section')

            </div>
            <!-- /#page-wrapper -->
        </div>
    </div>
@stop

