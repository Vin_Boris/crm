<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
    <title>Приложение №1 к договору №{{ $order->id  }}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
</head>

<body style="font-family: Arial; font-size: 7.5pt; ">
<style type="text/css" >
    table{
        border-collapse:collapse;
        border:1px solid #000;
    }
    .noborder{
        border:0px;
    }
</style>

    <p style="text-align: center; font-size: 7pt; font-weight: bold;">Приложение №1 к Договоруоказанияуслуг No №{{ $order->id  }} от {{ \Carbon\Carbon::parse( $order->created_at )->format( 'd.m.Y' ) }}
    </p>

    <div style="width:100%;">
        <p align=left > <strong>г Москва</strong> <strong>Сторона, именуемая в договоре "Турагент":</strong>

            <span class="T14">: </span><span class="T13">
        {{ $order->user->office->legal_entity->fullname()  }}, в лице {{ $order->user->fullname()  }}, действующго(ей)</span>
            <span class="T15"> на основании доверенности. Юр. адрес: (место нахождения): {{ $order->user->office->legal_entity->address }},</span>
            <span class="T13"> ОГРН {{ $order->user->office->legal_entity->ogrn }}, </span><span class="T18">ИНН </span>
            <span class="T12">{{ $order->user->office->legal_entity->inn }}</span><span class="T19">, р/с </span><span class="T12">{{ $order->user->office->legal_entity->account->rs }} в {{ $order->user->office->legal_entity->account->bank }}, к/с {{ $order->user->office->legal_entity->account->ks }}</span>
            <span class="T19">, БИК</span><span class="T12"> {{ $order->user->office->legal_entity->account->bik }},</span>
            <span class="T48">be</span><span class="T19">@</span><span class="T48">touronliner</span>
            <span class="T19">.</span><span class="T48">ru</span><span class="T19">, тел. 8(495)777-09-71</span>


        </p>

        <p align=left ><strong>Сторона, именуемая в договоре "Турист":</strong>

            <span class="T68">{{ $order->client_for_contract->fullname()  }}
                паспорт {{ $order->client_for_contract->passport  }}
                тел: {{ $order->client_for_contract->phone  }};
            </span>

        <p style="text-align: center; margin-top: 20px;"><strong>ЗАЯВКА НА БРОНИРОВАНИЕ ТУРПРОДУКТА</strong></p>
    </div>
    <p >1. ТУРИСТЫ:</p>

    <table cellpadding="3" cellspacing="0" width="100%" border=1 >
        <tr>
            <td rowspan="2" >ФИО + (транскрипция в загранпаспорте гражданина
                РФ для выезда из РФ и въезда в РФ)
            </td>
            <td rowspan="2" >Дата рождения</td>
            <td colspan="2" >Паспорт</td>
            <td colspan="2" >Продолжительность путешествия*</td>
        </tr>
        <tr>
            <td >серия, N</td>
            <td >действует до</td>
            <td >начало</td>
            <td >окончание</td>
        </tr>

        @foreach( $order->clients  as $client )
        <tr style="font-weight: bold;">
            <td > {{ $client->client->fullname() }} ({{ $client->client->fullname_eng() }} )</td>
            <td > {{ \Carbon\Carbon::parse( $client->client->birthday )->format( 'd.m.Y' ) }}</td>
            <td >{{ $client->client->international_passport }}</td>
            <td >{{ $client->client->int_passport_till ? \Carbon\Carbon::parse( $client->int_passport_till )->format( 'd.m.Y' ) : '' }}</td>
            <td >{{ $client->date_from}}</td>
            <td >{{ $client->date_to}}</td>
        </tr>
        @endforeach

    </table>

    <p>2. Размещение по маршруту:</p>

    <table cellpadding="3" cellspacing="0" width="100%" border=1  >
        <tr>
            <td >Страна</td>
            <td >Город</td>
            <td >Отель/категория (по каталогу ТО) / Тип номера</td>
            <td >Период пребывания (с-по)</td>
            <td >Питание</td>
        </tr>
        <tr style="font-weight: bold;">
            <td >{{ @$order->accomodations->first()->country->name }}</td>
            <td >{{ @$order->accomodations->first()->town->name }}</td>
            <td >
                {{ @$order->accomodations->first()->hotel->name }} / {{ $order->accomodations->first()->hotel->stars }}*
               ( {{ @$order->accomodations->first()->accomodation->name }}  )
            </td>
            <td >{{ $order->accomodations->first()->date_from }}</td>
            <td >{{ @$order->accomodations->first()->date_to }}</td>
            <td >{{ @$order->accomodations->first()->food->name }}</td>
        </tr>
    </table>

    <br />

    <p >3. Услуги: 3.1. Трансферы:</p>

    <table cellpadding="3" cellspacing="0" width="100%" border=1  >
        <tr>
            <td  align="center">Город</td>
            <td  align="center">Назначение</td>
            <td  align="center">Тип</td>
            <td  align="center">Кол.тур.</td>
        </tr>
        @foreach( $order->transfers as $transfer )
        <tr style="font-weight: bold;">
            <td >{{ $transfer->town->name }}</td>
            <td >{{ $transfer->direction }}</td>
            <td >{{ $transfer->type_name }}</td>
            <td >{{ $transfer->count }}</td>
        </tr>
        @endforeach
    </table>
    <br />

    <p > 3.2. Перевозки:</p>

    <table cellpadding="3" cellspacing="0" width="100%" border=1  >
        <tr>
            <td  align="center">Город отправления</td>
            <td  align="center">Город прибытия.</td>
            <td  align="center">Назначение</td>
            <td  align="center">Комментарии</td>
            <td  align="center">Кол.тур.</td>
        </tr>
        @foreach( $order->tickets as $ticket )
            <tr style="font-weight: bold;">
                <td >{{ @$ticket->from_town->name }}</td>
                <td >{{ @$ticket->to_town->name }}</td>
                <td >{{ $ticket->type_name }}</td>
                <td></td>
                <td >{{ $ticket->count }}</td>
            </tr>
        @endforeach
    </table>
    <br />

    <p > 3.3. Страхование:</p>

    <table cellpadding="3" cellspacing="0" width="100%" border=1  >
        <tr>
            <td  align="center">Тип страховки</td>
            <td  align="center">Комментарии</td>
            <td  align="center">Количество туристов</td>
        </tr>
        @foreach( $order->insurance as $insurance )
            <tr style="font-weight: bold;">
                <td >{{ $insurance->type_name }}</td>
                <td></td>
                <td >{{ $insurance->count }}</td>
            </tr>
        @endforeach
    </table>
    <br />

    <p >3.2. Доп. услуги:</p>

    <table cellpadding="3" cellspacing="0" width="100%" border=1  >
        <tr>
            <td  align="center">Описание</td>
            <td  align="center">Количество</td>
            <td  align="center">Цена</td>
        </tr>
        @foreach( $order->services as $service )
            <tr style="font-weight: bold;">
                <td >{{ $service->name }}</td>
                <td>{{ $service->count }}</td>
                <td >{{ $service->cost }}</td>
            </tr>
        @endforeach
    </table>

    <br />
    <p >3.3. Примечания:</p>
    <table cellpadding="3" cellspacing="0" width="100%" >
        <tr><td >{{ strip_tags( $order->description ) }}</td></tr>
    </table>

    <p >4. Общая стоимость турпродукта (в рублях):</p>
    <table cellpadding="3" cellspacing="0" width="100%" border=1  >
        <tr>
            <td ><strong>Общая цена турпродукта составляет:</strong>
            </td>
            <td >{{ $order->cost }}</td>
        </tr>
        <tr>
            <td ><strong>Стоимость виз:</strong>
            </td>
            <td >{{ $order->visas_price }}</td>
        </tr>
        <tr>
            <td ><strong>Стоимость доп.услуг составляет:</strong>
            </td>
            <td >{{ $order->services_price }}</td>
        </tr>
        <tr>
            <td ><strong>Скидка</strong> (Внимание! Скидки на дополнительные оплаты не распространяются):&nbsp;&nbsp;&nbsp;{{ $order->discount }} %</td>
            <td >{{ $order->discount_sum }}</td>
        </tr>
        <tr>
            <td ><strong>Предоплата (не менее 50%):</strong> </td>
            <td ></td>
        </tr>
        <tr>
            <td ><strong>Общая стоимость турпродукта составляет:</strong></td>
            <td >{{ $order->cost - $order->discount_sum }}</td>
        </tr>
        <tr>
            <td ><strong>Полная оплата турпродукта производится в срок до:</strong></td>
            <td > {{ $order->full_payment_date ? \Carbon\Carbon::parse( $order->full_payment_date )->format('d.m.Y') : ''  }}</td>
        </tr>
    </table>

    <br /><br />

    <p >5. Информация о туроператоре - непосредственном исполнителе услуг, входящих в турпродукт:</p>

    <table cellpadding="10" cellspacing="0" border=1  width="100%" >
        <tr>
            <td>Реестровый номер: {{ $order->tourop->efrt_number }}
                место нахождения: {{ $order->tourop->address }}
                Полное наименование: {{ $order->tourop->full_name }}
                Сокращенное наименование: {{ $order->tourop->short_name }}
                Адрес официального сайта в сети "Интернет": {{ $order->tourop->www }}
                ИНН: {{ $order->tourop->inn }}
                ОГРН: {{ $order->tourop->ogrn }}
                Членство туроператора, осуществляющего деятельность в сфере выездного туризма, в объединении туроператоров в сфере выездного туризма : Да
                Адреса структурных подразделений :  {{ $order->tourop->units_address }}
                Общая цена туристского продукта в сфере выездного туризма: {{ $order->tourop->full_product_price }}
                Количество туристов в сфере выездного туризма за предыдущий год : {{ $order->tourop->prev_year_tourists_count }}
                Размер уплаченного взноса в резервный фонд:  {{ $order->tourop->reserve_amount }}
                Фактический размер фонда персональной ответственности туроператора:  {{ $order->tourop->responsibility_amount }}
                Размер ежегодного взноса, перечисленного туроператором в фонд персональной ответственности туроператора в отчетном году: {{ $order->tourop->year_contribution_amount }}
                Общий размер финансового обеспечения:  {{ $order->tourop->finsupply_amount_full }}

                Документы: {{ $order->tourop->finsupply_docs_text }}

                Сфера туроператорской деятельности: {{ $order->tourop->sphere_of_activity }}

                Дата и номер приказа Ростуризма о внесении сведений в единый федеральный реестр туроператоров

                Номер приказа: {{ $order->tourop->order_number }}
                Дата приказа: {{ $order->tourop->order_date }}
                Номер выданного свидетельства: {{ $order->tourop->certificate_number }}

            </td>
        </tr>

    </table>



    <br />

    <p >6. Турист заявляет о том, что:</p>
    <table cellpadding="3" cellspacing="0" width="100%" border=1  >
        <tr>
            <td >7.1. С правилами страхования от невыезда ознакомлен(а):</td>
            <td >Подпись: _____________________________</td>
        </tr>
        <tr>
            <td >а) Согласен (согласна) оформить страховку от невыезда:</td>
            <td >Подпись: _____________________________</td>
        </tr>
        <tr>
            <td >б) Отказываюсь оформить страховку от невыезда:</td>
            <td >Подпись: _____________________________</td>
        </tr>
        <tr>
            <td >7.2. С информацией о стране пребывания, условиях и особенностях осуществления путешествия, правилами безопасности и поведения в стране пребывания ознакомлен(а)</td>
            <td >Подпись: _____________________________</td>
        </tr>
        <tr>
            <td > 7.3. Информацию о финансовом обеспечении туроператора, основаниях и порядке выплаты страхового возмещения или уплаты денежной суммы по банковской гарантии получил(а)</td>
            <td >Подпись: _____________________________</td>
        </tr>
    </table>

    <br /><br />

<table cellpadding="0" cellspacing="0" width="100%" align="center" style="border:0px;" >
    <tr>
        <td width="49%" valign="top">
            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>ТУРАГЕНТ</strong></p>
            _____________________/{{$order->user->short_name()}}./<br />
            М.П.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<em>подпись</em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<em>расшифровка</em>
        </td>
        <td width="2%"></td>
        <td width="49%" valign="top">
            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>ТУРИСТ</strong></p>
            _____________________/{{$order->client_for_contract->short_name() }}./<br />
            М.П.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<em>подпись</em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<em>расшифровка</em>
        </td>
    </tr>
</table>
</center>
</body>

</html>
