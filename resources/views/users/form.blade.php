@extends('layouts.dashboard')

@section('page_heading','Форма пользователя')

@section('section')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="row">
        <div class="col-sm-12">
            <form method="POST" action="{{ url ('/users/save' . ( $user->id > 0 ? '/' . $user->id : '' ) ) }}">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                 <div class="form-group">
                    <label>Фамилия</label>
                    <input class="form-control" name="lastname" value="{{ $user->last_name }}" >
                </div>

                <div class="form-group">
                    <label>Имя</label>
                    <input class="form-control" name="firstname" value="{{ $user->first_name }}" >
                </div>

                <div class="form-group">
                    <label>Отчество</label>
                    <input class="form-control" name="middlename" value="{{ $user->middle_name }}" >
                </div>

                <div class="form-group">
                    <label>Пол</label>

                    <select class="form-control" name="sex" >
                        <option value="0" {{ ( $user->sex == 0 ? 'selected' : '' )  }} >Мужской</option>
                        <option value="1" {{ ( $user->sex == 1 ? 'selected' : '' )  }} >Женский</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Email</label>
                    <input class="form-control" name="birthdate" value="{{ $user->birthdate }}" >
                </div>

                <div class="form-group">
                    <label>Логин</label>
                    <input class="form-control" name="login" value="{{ $user->login }}" >
                </div>
                <div class="form-group">
                    <label>Пароль</label>
                    <input class="form-control" name="password" value="{{ $user->password ? '******' : '' }}" >
                </div>

                <div class="form-group">
                    <label>Офис</label>

                    <select class="form-control" name="office" >
                        <option value="0" >не выбрано </option>
                        @foreach( $offices as $office )
                            <option value="{{$office->id}}" {{ ( $office->id == $user->office_id ? 'selected' : '' )  }} >{{ $office->name  }}</option>
                        @endforeach
                    </select>
                </div>


                <div class="form-group">
                    <label>Email</label>
                    <input class="form-control" name="email" value="{{ $user->email }}" >
                </div>

                <div class="form-group">
                    <label>Телефон</label>
                    <input class="form-control" name="phone" value="{{ $user->phone }}">
                </div>

                <div >
                    <label class="checkbox-inline">
                        <input type="checkbox" name="isactive" {{ ( !$user->exists || $user->isactive == 1 ? 'checked' : '' )  }} > Активность
                    </label>
                </div>

                <br/>


                <button type="submit" class="btn btn-default">Сохранить</button>

            </form>
        </div>
    </div>

@endsection