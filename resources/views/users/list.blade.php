@extends('layouts.dashboard')

@section('page_heading','Список пользователей')

@section('section')


    <div class="row">
        <div class="col-sm-12">

            <a href="{{ url ('/users/edit' ) }}" class="btn btn-primary" >
                <i class="fa fa-plus" ></i> Добавить пользователя
            </a>

            <br/><br/>

            @if( count( $data ) >0 )

                <table class="table table-bordered">
                    <thead>
                        <th></th>
                        <th>ФИО</th>
                        <th>Офис</th>
                        <th>Телефон</th>
                        <th>Email</th>
                        <th>Активность</th>
                    </thead>
                    <tbody>
                    @foreach( $data as $row )
                        <tr >
                            <td><a href="{{ url ('/users/edit/' . $row->id ) }}" ><i class="fa fa-edit fa-fw "></i></a></td>
                            <td>{{ $row->fullname() }}</td>
                            <td>{{ $row->offices ? $row->offices->name : 'не выбрано' }}</td>
                            <td>{{ $row->phone }}</td>
                            <td>{{ $row->email }}</td>
                            <td>{{ $row->isactive == 1 ? 'Активен' : 'Неактивен' }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <h3>Ничего не найдено</h3>
            @endif

        </div>
    </div>

@endsection