@extends('layouts.dashboard')

@section('page_heading','Список туроператоров')

@section('section')



    <div class="row">
        <div class="col-sm-12">

            <a href="{{ url ('/tour_operators/edit' ) }}" class="btn btn-primary" >
                <i class="fa fa-plus" ></i> Добавить туроператора
            </a>

            <br/> <br/>

            @if( count( $data ) >0 )

            <table class="table table-bordered">
                <thead>
                    <th></th>
                    <th>№ ЕФРТ</th>
                    <th>Короткое название</th>
                    <th>WWW </th>
                </thead>
                <tbody>
                @foreach( $data as $row )
                <tr >
                    <td><a href="{{ url ('/tour_operators/edit/' . $row->id ) }}" ><i class="fa fa-edit fa-fw "></i></a></td>
                    <td>{{ $row->efrt_number }}</td>
                    <td>{{ $row->short_name }}</td>
                    <td><a href="{{ $row->www }}" target="_blank">{{ $row->www }}</a></td>
                </tr>
                @endforeach
                </tbody>
            </table>
            @else
                <h3>Ничего не найдено</h3>
            @endif
        </div>
    </div>

@endsection