@extends('layouts.dashboard')

@section('page_heading','Форма туроператора')

@section('section')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="col-sm-12">
            <form method="POST" action="{{ url ('/tour_operators/save' . ( $tourop->id > 0 ? '/' . $tourop->id : '' ) ) }}">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group">
                    <label>Номер ЕФРТ</label>
                    <input class="form-control" name="efrt_number" value="{{ $tourop->efrt_number }}" >
                </div>


                <div class="form-group">
                    <label>Короткое название</label>
                    <input class="form-control" name="short_name" value="{{ $tourop->short_name }}" >
                </div>

                <div class="form-group">
                    <label>Полное наименование</label>
                    <input class="form-control" name="full_name" value="{{ $tourop->full_name }}" >
                </div>

                <div class="form-group">
                    <label>Адрес</label>
                    <input class="form-control" name="address" value="{{ $tourop->address }}" >
                </div>

                <div class="form-group">
                    <label>WWW</label>
                    <input class="form-control" name="www" value="{{ $tourop->www }}" >
                </div>

                <div class="form-group">
                    <label>ИНН</label>
                    <input class="form-control" name="inn" value="{{ $tourop->inn }}">
                </div>

                <div class="form-group">
                    <label>ОГРН</label>
                    <input class="form-control" name="ogrn" value="{{ $tourop->ogrn }}">
                </div>

                <div class="form-group">
                    <label>Членство туроператора, осуществляющего деятельность в сфере выездного туризма, в объединении туроператоров в сфере выездного туризма</label>
                    <input class="form-control" name="is_member" type="checkbox" {{ $tourop->is_member == 1 ? 'checked' : '' }}>
                </div>

                <div class="form-group">
                    <label>Адреса структурных подразделений</label>
                    <textarea class="form-control" name="unit_address" rows="3">{{ $tourop->units_address }}</textarea>
                </div>

                <div class="form-group">
                    <label>Общая цена туристского продукта в сфере выездного туризма </label>
                    <input class="form-control" name="full_product_price" value="{{ $tourop->full_product_price }}">
                </div>

                <div class="form-group">
                    <label>Количество туристов в сфере выездного туризма за предыдущий год </label>
                    <input class="form-control" name="prev_year_tourists_count" value="{{ $tourop->prev_year_tourists_count }}">
                </div>

                <div class="form-group">
                    <label>Размер уплаченного взноса в резервный фонд  </label>
                    <input class="form-control" name="reserve_amount" value="{{ $tourop->reserve_amount }}">
                </div>

                <div class="form-group">
                    <label>Фактический размер фонда персональной ответственности туроператора   </label>
                    <input class="form-control" name="responsibility_amount" value="{{ $tourop->responsibility_amount }}">
                </div>

                <div class="form-group">
                    <label>Размер ежегодного взноса, перечисленного туроператором в фонд персональной ответственности туроператора в отчетном году    </label>
                    <input class="form-control" name="year_contribution_amount" value="{{ $tourop->year_contribution_amount }}">
                </div>

                <div class="form-group">
                    <label>Финансовое обеспечение </label>
                    <input class="form-control" name="finsupply_amount_full" value="{{ $tourop->finsupply_amount_full }}">
                </div>

                <div class="form-group">
                    <label>Документы </label>
                    <textarea class="form-control" name="finsupply_docs_text" > {{ $tourop->finsupply_docs_text }}</textarea>
                </div>

                <button type="submit" class="btn btn-default">Сохранить</button>

            </form>
        </div>
    </div>

@endsection