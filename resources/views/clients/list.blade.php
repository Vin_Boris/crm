@extends('layouts.dashboard')

@section('page_heading','Список клиентов')

@section('section')



    <div class="row">
        <div class="col-sm-12">

            <form method="GET" >
                @section ('filter_panel_title','Фильтр')
                @section ('filter_panel_body')

                    <div class="row form-group" >
                        <div class="col-sm-2">
                            <input class="form-control" placeholder="Фамилия" name="last_name" value="{{$filter['last_name']}}">
                        </div>
                        <div class="col-sm-2">
                            <input class="form-control" placeholder="Имя" name="first_name" value="{{$filter['first_name']}}">
                        </div>

                        <div class="col-sm-2">
                            <input class="form-control" placeholder="Отчество" name="middle_name" value="{{$filter['middle_name']}}">
                        </div>

                        <div class="col-sm-2">
                            <input class="form-control datepicker_input" placeholder="даты поездок с" name="tour_date_from" value="{{$filter['tour_date_from']}}">
                        </div>

                        <div class="col-sm-2">
                            <input class="form-control datepicker_input" placeholder="даты поездок по" name="tour_date_to" value="{{$filter['tour_date_to']}}">
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-sm-2">
                            <input class="form-control datepicker_input" placeholder="даты заявок с" name="order_date_from" value="{{$filter['order_date_from']}}">
                        </div>

                        <div class="col-sm-2">
                            <input class="form-control datepicker_input" placeholder="даты заявок по" name="order_date_to" value="{{$filter['order_date_to']}}">
                        </div>

                        <div class="col-sm-2">
                            <select class="form-control" name="user_id">
                                <option value="0">Выберите менеджера</option>
                                @foreach( $users as $user )
                                    <option value="{{$user->id}}" {{ $filter['user_id'] == $user->id ? 'selected' : ''  }}>{{$user->fullname()}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-sm-2">

                            <div class="form-group input-group ">
                                <input class="form-control"  type="hidden" name="country_id"  value="{{$filter['country_id']}}"  />
                                <input class="form-control filter_country_autocomplete" type="text"  placeholder="Введите страну" value="{{$filter['country_name']}}"  />
                            </div>

                        </div>

                    </div>
                @endsection

                @section ('filter_panel_footer')

                    <button type="submit" class="btn btn-default" >Поиск</button>

                    <button type="button" class="btn btn-default filter_reset" >Сброс</button>

                @endsection

                @include('widgets.panel', array('header'=>true ,'footer' => true , 'as'=>'filter'))

            </form>

            <a href="{{ url ('/clients/edit' ) }}" class="btn btn-primary" >
                <i class="fa fa-plus" ></i> Добавить клиента
            </a>

            <br/><br/>

            @if( count( $data ) >0 )

                <table class="table table-bordered">
                    <thead>
                        <tr >
                            <td></td>
                            <td>Имя</td>
                            <td>Отчество</td>
                            <td>Фамилия</td>
                            <td>Пол</td>
                            <td>Дата рождения</td>
                            <td>email</td>
                            <td>Телефон</td>
                            <td>Номер паспорта</td>
                            <td>Номер международного паспорта</td>
                            <td>Пользователь</td>
                            @if( $add_to_order )
                            <td>Добавить в заявку</td>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                    @foreach( $data as $row )
                        <tr >
                            <td><a href="{{ url ('/clients/edit/' . $row->id ) }}" ><i class="fa fa-edit fa-fw "></i></a></td>
                            <td>{{ $row->first_name }}</td>
                            <td>{{ $row->middle_name }}</td>
                            <td>{{ $row->last_name }}</td>
                            <td>{{ $row->sex }}</td>
                            <td>{{ $row->birthday }}</td>
                            <td>{{ $row->email }}</td>
                            <td>{{ $row->phone }}</td>
                            <td>{{ $row->passport }}</td>
                            <td>{{ $row->international_passport }}</td>
                            <td>{{ $row->user->fullname() }}</td>
                            @if( $add_to_order )
                                <td align="center">
                                    <button type="button" class="btn btn-success btn-circle add_client_to_order" data-id="{{$row->id}}"><i class="fa fa-plus"></i>
                                    </button>
                                </td>
                            @endif
                            @if( $add_to_order_contract )
                                <td align="center">
                                    <button type="button" class="btn btn-success btn-circle add_to_order_contract" data-id="{{$row->id}}"><i class="fa fa-plus"></i>
                                    </button>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <h3>Ничего не найдено</h3>
            @endif
        </div>
    </div>


@endsection