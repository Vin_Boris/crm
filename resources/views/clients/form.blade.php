@extends('layouts.dashboard')

@section('page_heading','Форма клиента')

@section('section')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="col-sm-12">
            <form method="POST" action="{{ url ('/clients/save/' . ( $client->id > 0 ? $client->id : '' ) ) }}">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group">
                    <label>Фамилия</label>
                    <input class="form-control" name="last_name" id="last_name" value="{{ $client->last_name }}" >
                </div>

                <div class="form-group">
                    <label>Имя</label>
                    <input class="form-control" name="first_name" id="first_name" value="{{ $client->first_name }}" >
                </div>

                <div class="form-group">
                    <label>Отчество</label>
                    <input class="form-control" name="middle_name" value="{{ $client->middle_name }}" >
                </div>

                <div class="form-group">
                    <label>Имя (транскрипция)</label>
                    <input class="form-control" id="first_name_eng" name="first_name_eng" editing=0 value="{{ $client->first_name_eng }}" >
                </div>

                <div class="form-group">
                    <label>Фамилия (транскрипция)</label>
                    <input class="form-control" id="last_name_eng" name="last_name_eng" editing=0 value="{{ $client->last_name_eng }}" >
                </div>

                <div class="form-group">
                    <label>Пол</label>
                    <select name="sex" class="form-control" >
                        <option value="0">Мужской</option>
                        <option value="1">Женский</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Email</label>
                    <input class="form-control" name="email" value="{{ $client->email }}" >
                </div>

                <div class="form-group">
                    <label>Телефон</label>
                    <input class="form-control" name="phone" value="{{ $client->phone }}">
                </div>

                <div class="form-group">
                    <label>Дата рождения</label>
                    <input class="form-control datepicker_input" name="birthday"   value="{{ $client->birthday }}">
                </div>

                <div class="form-group">
                    <label>Номер паспорта</label>
                    <input class="form-control" name="passport" value="{{ $client->passport }}">
                </div>

                <div class="form-group">
                    <label>Паспорт выдан</label>
                    <textarea class="form-control" name="passport_gift" rows="3" >{{ $client->passport_gift }}</textarea>
                </div>

                <div class="form-group">
                    <label>Номер международного паспорта</label>
                    <input class="form-control" name="international_passport" value="{{ $client->international_passport }}">
                </div>

                <div class="form-group">
                    <label>Международный паспорт выдан</label>
                    <textarea class="form-control" name="international_passport_gift" rows="3" >{{ $client->international_passport_gift }}</textarea>
                </div>

                <div class="form-group">
                    <label>Срок действия международного паспорта</label>
                    <input class="form-control datepicker_input" name="int_passport_till" value="{{ $client->int_passport_till }}">
                </div>

                <div class="form-group">
                    <label>Виза</label>
                    <input class="checkbox-inline" type="checkbox" name="have_visa" {{ $client->have_visa == 1 ? 'checked' : '' }}>
                </div>

                <div class="form-group">
                    <label>Виза дествует до</label>
                    <input class="form-control datepicker_input"  name="visa_till" value="{{ $client->visa_till }}" >
                </div>

                <div class="form-group">
                    <label>Тип клиента</label>
                    <select name="type" class="form-control" >
                        <option value="0">Эконом</option>
                        <option value="1">VIP</option>
                        <option value="2">Цена/Качество</option>
                    </select>
                </div>

                <button type="submit" class="btn btn-default">Сохранить</button>

            </form>
        </div>
    </div>



@endsection