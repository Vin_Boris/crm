function OrderForm(){
    this.window = window;

    this.client_count = 0;
    this.amount = 0;

    this.open_tab =  function( url ){
        var win = window.open( url, '_blank');
        if (win) {
            win.focus();
        } else {
            //Browser has blocked it
            alert('Please allow popups for this website');
        }
    };

    this.init_accomodation_fields = function( container ){

        var $towns = $(container).find('.town_autocomplete');
        var $countries = $(container).find('.country_autocomplete');
        var $hotels = $(container).find('.hotel_autocomplete');
        var $acc = $(container).find('.acc_autocomplete');
        var $foods = $(container).find('.food_autocomplete');
        var $from = $(container).find('.date_from');
        var $to = $(container).find('.date_to');

        this.autocomplete(
            $countries,
            '/countries',
            '/countries/edit',
            function( item ){
                $towns.autocomplete( "option", "source", '/towns/' + item.id );
                $towns.prop( 'disabled', false );
            } );

        this.autocomplete(  $towns , '/towns' , '/towns/edit',
            function( item ){
                $hotels.autocomplete( "option", "source", '/hotels/' + item.id );
                $hotels.prop( 'disabled', false );
            },
            function( save_path ){
                return save_path + '/' + $countries.prev().val()  ;

        } );

        this.autocomplete(  $hotels , '/hotels' , '/hotels/edit', null, function( save_path ){
            return save_path + '/' + $towns.prev().val() ;
        });

        this.autocomplete(  $acc , '/accomodations' , '/accomodations/edit');
        this.autocomplete(  $foods , '/food' , '/food/edit');

        $from.datepicker({
            dateFormat: "dd.mm.yy"
        });
        $to.datepicker({
            dateFormat: "dd.mm.yy"
        });

    }

    this.autocomplete = function( input, data_path, save_path, select, save_callback ) {

        var opt = {
            source : data_path,
            create: function() {
                $(this).data('ui-autocomplete')._renderItem = function( ul, item ) {
                    return $( "<li>" )
                        .attr( "data-id", item.id )
                        .append( item.name )
                        .appendTo( ul );
                }
            },
            select: function( event, ui ) {
                event.preventDefault();
                $( event.target ).val( ui.item.name );
                $( event.target ).prev().val( ui.item.id );
                $(  event.target ).next().children().first().attr('class','btn btn-default');

                if( select !== undefined && select ){
                    select( ui.item );
                }
            }
        };

        $( input ).autocomplete( opt );



        $(input).on( 'input', function(){

            var save    = $(this).next().children().first();

            if( $(this).val() == "" ){
                $( this ).prev().val( 0 );
                $( save ).attr('class','btn btn-default');
            }else{
                $( save ).attr('class','btn btn-success');
            }

        });

        var save    = $(input).next().children().first();

        $( save ).on('click', function(){

            if( $(input).prop('disabled') !== true ){
                var name    = $(input).val();
                var id      = $(input).prev().val();
                var path    = save_path;
                if( save_callback !== undefined ){
                    path = save_callback( save_path );
                }

                $.post( path + '/' + id , { _token : $('meta[name="_token"]').attr('content') ,name : name } , function( responce ){
                    $(input).prev().val( responce.id );
                    $( save ).attr('class','btn btn-default');

                    if(  select !== undefined && select ){
                        select( responce );
                    }

                } );
            }


        });

    }

}

OrderForm.prototype.init =  function(){
    this.client_count = $( '#tourists_row_container' ).find('.tourist_row').length;
    var that = this;
    that.amount =  $('[name="price"]').val();
    var form = $('#order_form');

    $( form ).on( 'input', '[name*="[cost]"]', function( e ){
        that.amount = 0;
        $('#order_form').find('[name*="[cost]"]').each( function(){
            that.amount += $(this).val()*1;
        });
        $('[name="price"]').val( that.amount);
    } );

    $( form ).on( 'input', '[name="currency_rate"]', function( e ){
        $('[name="price_rub"]').val( that.amount*$(this).val() );
    });
};

OrderForm.prototype.add_client =  function(){
   this.open_tab( '/clients?add_to_order=1' );
};


OrderForm.prototype.remove_client =  function( id ){
    var container = $('#tourists_row_container');
    $( container ).find('[data-client="' + id + '"]').remove();
    if( $( container ).find('.tourist_row').length == 0){
        $( container ).find('#tourists_row_empty_message').show();
    }
};

OrderForm.prototype.insert_client = function( id ){

    var container = $('#tourists_row_container');
    var that = this;

    if( $( container ).find('.tourist_row').length == 0){
        $( container ).find('#tourists_row_empty_message').hide();
    }

    if( $( container ).find('[data-client="' + id + '"]').length == 0 ){
        $.get('/orders/get-client-row/' + id, function( data ){
            $(container).append( data );

            that.client_count++;
        } );
    }
};

OrderForm.prototype.init_accomodations = function(){
    var that = this;
    $('#accomodations').find('.accomodation_row').each( function(){
        that.init_accomodation_fields( $(this) );
    });
};

OrderForm.prototype.add_accomodation =  function(){
    var container = $('#accomodation_row_container');

    if( $( container ).find('.accomodation_row').length == 0){
        $( container ).find('#accomodation_row_empty_message').hide();
    }

    $(container).append( $('#accomodation_row_template').html() );

    var added_row = $(container).find('.accomodation_row').last();

    this.init_accomodation_fields( added_row );

    $(added_row).find('[name="accomodations[count][]"]').val( this.client_count );
};

OrderForm.prototype.remove_accomodation =  function( button ){
    var container = $('#accomodation_row_container');
   $(button).parent().parent().remove();
    if( $( container ).find('.accomodation_row').length == 0){
        $( container ).find('#accomodation_row_empty_message').show();
    }
};

OrderForm.prototype.init_transfers = function(){
    var that = this;
    var $country = $('#accomodation_row_container').find('.country_autocomplete').first();
    $('#transfers').find('.transfer_row').each( function(){

        that.autocomplete( $(this).find('.transfer_town_autocomplete'), '/towns' , '/towns/edit',
            null,
            function( save_path ){
                return save_path + '/' + $country.prev().val() ;
            } );
    });
};

OrderForm.prototype.add_transfer = function(){
    var container = $('#transfer_row_container');
    var $country = $('#accomodation_row_container').find('.country_autocomplete').first();

    if( $( container ).find('.transfer_row').length == 0){
        $( container ).find('#transfer_row_empty_message').hide();
    }

    $(container).append( $('#transfer_row_template').html() );

    var added_row =  $(container).find('.transfer_row').last();

    this.autocomplete( $( added_row ).find('.transfer_town_autocomplete') , '/towns' , '/towns/edit',
        null,
        function( save_path ){
            return save_path + '/' + $country.prev().val() ;
        } );

    $(added_row).find('[name="transfer[count][]"]').val( this.client_count );
};

OrderForm.prototype.remove_transfer =  function( button ){
    var container = $('#transfer_row_container');
    $(button).parent().parent().remove();
    if( $( container ).find('.transfer_row').length == 0){
        $( container ).find('#transfer_row_empty_message').show();
    }
};

OrderForm.prototype.init_tickets = function(){
    var that = this;
    var $country = $('#accomodation_row_container').find('.country_autocomplete').first();
    $('#tickets').find('.ticket_row').each( function(){

        that.autocomplete( $(this).find('.ticket_town_autocomplete'), '/towns' , '/towns/edit',
            null,
            function( save_path ){
                return save_path + '/' + $country.prev().val() ;
            } );
    });
};

OrderForm.prototype.add_ticket = function(){
    var container = $('#tickets_row_container');
    var $country = $('#accomodation_row_container').find('.country_autocomplete').first();

    if( $( container ).find('.ticket_row').length == 0){
        $( container ).find('#ticket_row_empty_message').hide();
    }

    $(container).append( $('#ticket_row_template').html() );

    var added_row =  $(container).find('.ticket_row').last();

    this.autocomplete( $( added_row ).find('.ticket_town_autocomplete'), '/towns' , '/towns/edit',
        null,
        function( save_path ){
            return save_path + '/' + $country.prev().val() ;
        } );

    $(added_row).find('[name="tickets[count][]"]').val( this.client_count );

};

OrderForm.prototype.remove_ticket =  function( button ){
    var container = $('#tickets_row_container');
    $(button).parent().parent().remove();
    if( $( container ).find('.ticket_row').length == 0){
        $( container ).find('#ticket_row_empty_message').show();
    }
};


OrderForm.prototype.add_insurance = function(){
    var container = $('#insurance_row_container');

    if( $( container ).find('.insurance_row').length == 0){
        $( container ).find('#insurance_row_empty_message').hide();
    }

    $(container).append( $('#insurance_row_template').html() );

    var added_row =  $(container).find('.insurance_row').last();
    $(added_row).find('[name="insurance[count][]"]').val( this.client_count );

};

OrderForm.prototype.remove_insurance =  function( button ){
    var container = $('#insurance_row_container');
    $(button).parent().parent().remove();
    if( $( container ).find('.insurance_row').length == 0){
        $( container ).find('#insurance_row_empty_message').show();
    }
};


OrderForm.prototype.init_visa = function(){

    this.autocomplete( $('#visas').find('.visas_country_autocomplete'),
        '/countries',
        '/countries/edit');
};

OrderForm.prototype.add_visa = function(){
    var container = $('#visas_row_container');

    if( $( container ).find('.visas_row').length == 0){
        $( container ).find('#visas_row_empty_message').hide();
    }

    $(container).append( $('#visas_row_template').html() );

    var added_row =  $(container).find('.visas_row').last();

    this.autocomplete(
        $( added_row ).find('.visas_country_autocomplete'),
        '/countries',
        '/countries/edit'
    );

    $(added_row).find('[name="visas[count][]"]').val( this.client_count );

};

OrderForm.prototype.remove_visa =  function( button ){
    var container = $('#visas_row_container');
    $(button).parent().parent().remove();
    if( $( container ).find('.visas_row').length == 0){
        $( container ).find('#visas_row_empty_message').show();
    }
};


OrderForm.prototype.add_service = function(){
    var container = $('#services_row_container');

    if( $( container ).find('.service_row').length == 0){
        $( container ).find('#services_row_empty_message').hide();
    }

    $(container).append( $('#services_row_template').html() );

    var added_row =  $(container).find('.service_row').last();
    $(added_row).find('[name="services[count][]"]').val( this.client_count );

};

OrderForm.prototype.remove_service = function( button ){
    var container = $('#services_row_container');
    $(button).parent().parent().remove();
    if( $( container ).find('.service_row').length == 0){
        $( container ).find('#services_row_empty_message').show();
    }
};

OrderForm.prototype.add_client_to_contract = function () {
    this.open_tab( '/clients?add_to_order_contract=1' );
};

OrderForm.prototype.insert_client_to_contract = function ( id ) {

    $('#tourist_for_contract').removeClass('alert-danger');
    $('#tourist_for_contract').addClass('alert-info');

    $.get('/clients/info/' + id, function( data ){
        $('[name="tourist_for_contract"]').val(id);
        $('#tourist_for_contract').html( data.fullname + ' ' + data.birthday );

    } );
};



OrderForm.prototype.add_client_payment = function(){
    var container = $('#client_payments_row_container');

    if( $( container ).find('.client_payment_row').length == 0){
        $( container ).find('#client_payments_row_empty_message').hide();
    }
    $(container).append( $('#client_payments_template').html() );
    this.calc_fin_info();
};

OrderForm.prototype.remove_client_payment = function( button ){
    var container = $('#client_payments_row_container');
    $(button).parent().parent().remove();
    if( $( container ).find('.client_payment_row').length == 0){
        $( container ).find('#client_payments_row_empty_message').show();
    }
    this.calc_fin_info();
};


OrderForm.prototype.add_tourop_payment = function(){
    var container = $('#tourop_payments_row_container');

    if( $( container ).find('.tourop_payment_row').length == 0){
        $( container ).find('#tourop_payments_row_empty_message').hide();
    }
    $(container).append( $('#tourop_payments_template').html() );
    this.calc_fin_info();
};

OrderForm.prototype.remove_tourop_payment = function( button ){
    var container = $('#tourop_payments_row_container');
    $(button).parent().parent().remove();
    if( $( container ).find('.tourop_payment_row').length == 0){
        $( container ).find('#tourop_payments_row_empty_message').show();
    }
    this.calc_fin_info();
};

OrderForm.prototype.add_confirmation = function(){
    var container = $('#confirmations_row_container');

    if( $( container ).find('.confirmation_row').length == 0){
        $( container ).find('#confirmations_row_empty_message').hide();
    }
    $(container).append( $('#confirmations_template').html() );
    this.calc_fin_info();
};

OrderForm.prototype.remove_confirmation = function( button ){
    var container = $('#confirmations_row_container');
    $(button).parent().parent().remove();
    if( $( container ).find('.confirmation_row').length == 0){
        $( container ).find('#confirmations_row_empty_message').show();
    }
    this.calc_fin_info();
};

OrderForm.prototype.calc_fin_info = function(){

    var client_payments_container = $('#client_payments_row_container');
    var client_amount = 0;

    $( client_payments_container ).find('[name="client_payments[amount][]"]').each( function( index, item ){
        var sign = $( client_payments_container ).find('[name="client_payments[sign][]"] option:selected').get( index );

        if( $(sign).html() == '+' ){
            client_amount += Number( $(this).val() );
        }else{
            client_amount -= Number( $(this).val() );
        }

    } );

    $('#client_payment_amount').html( client_amount );

    var confirmations_container = $('#confirmations_row_container');
    var confirmations_amount = 0;

    $( confirmations_container ).find('[name="confirmations[amount_ue][]"]').each( function(){
        confirmations_amount += Number( $(this).val() );
    } );

    $('#confirmations_payment_amount').html( confirmations_amount );

    var tourop_container = $('#tourop_payments_row_container');
    var tourop_amount = 0;

    $( tourop_container ).find('[name="tourop_payments[amount][]"]').each( function(){
        tourop_amount += Number( $(this).val() );
    } );

    $('#tourop_payment_amount').html( tourop_amount );
};







