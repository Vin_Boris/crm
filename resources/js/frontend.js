
var order_form = new OrderForm();

$( document ).ready( function(){

    order_form.init();

   //Клиенты

    $('#add_client').click( function(){
        order_form.add_client();
    } );

    $('#add_to_tourist_to_contract').click( function(){
        order_form.add_client_to_contract();
    } );

    $(document).on( 'click','.remove_client', function(){
        order_form.remove_client( $(this).attr('data-remove_client') );
    } );

    $('.add_client_to_order' ).on('click', function () {
        window.opener.order_form.insert_client( $(this).attr('data-id') );
        window.opener.focus();
        window.close();
    });

    $('.add_to_order_contract' ).on('click', function () {
        window.opener.order_form.insert_client_to_contract( $(this).attr('data-id') );
        window.opener.focus();
        window.close();
    });

    //Размещения

    order_form.init_accomodations();

    $('#add_accomodation').click( function(){
        order_form.add_accomodation();
    } );


    $(document).on( 'click','.remove_accomodation', function(){
        order_form.remove_accomodation( $(this) );
    } );

    //Трансферы

    order_form.init_transfers();

    $('#add_transfer').click( function(){
        order_form.add_transfer();
    } );


    $(document).on( 'click','.remove_transfer', function(){
        order_form.remove_transfer( $(this) );
    } );

    //Перевозки

    order_form.init_tickets();

    $('#add_ticket').click( function(){
        order_form.add_ticket();
    } );


    $(document).on( 'click','.remove_ticket', function(){
        order_form.remove_ticket( $(this) );
    } );

    //Страхование

    $('#add_insurance').click( function(){
        order_form.add_insurance();
    } );


    $(document).on( 'click','.remove_insurance', function(){
        order_form.remove_insurance( $(this) );
    } );

    //Визы

    order_form.init_visa();

    $('#add_visa').click( function(){
        order_form.add_visa();
    } );


    $(document).on( 'click','.remove_visa', function(){
        order_form.remove_visa( $(this) );
    } );

    //Доп услуги

    $('#add_order_service').click( function(){
        order_form.add_service();
    } );


    $(document).on( 'click','.remove_service', function(){
        order_form.remove_service( $(this) );
    } );

    //Оплаты клиента

    $('#add_client_payment').click( function(){
        order_form.add_client_payment();
    } );


    $(document).on( 'click','.remove_client_payment', function(){
        order_form.remove_client_payment( $(this) );
    } );

    //Подтверждения

    $('#add_confirmation').click( function(){
        order_form.add_confirmation();
    } );


    $(document).on( 'click','.remove_confirmation', function(){
        order_form.remove_confirmation( $(this) );
    } );

    //Оплаты ТО
    $('#add_tourop_payment').click( function(){
        order_form.add_tourop_payment();
    } );


    $(document).on( 'click','.remove_tourop_payment', function(){
        order_form.remove_tourop_payment( $(this) );
    } );

    $('#order_form').on( 'submit', function( e ){

        var $save_message = $('#save_message');

         $.post( $(this).attr('action'), $(this).serializeArray(), function( response ){

             if( response.error !== undefined ){
                 $save_message.html( response.error );
                 $save_message.removeClass('alert-success');
                 $save_message.addClass('alert-danger');
             }else{
                 $save_message.html( response.message );
                 $save_message.removeClass('alert-danger');
                 $save_message.addClass('alert-success');
             }

             $save_message.removeClass('hidden');
             $('html, body').animate({
                 scrollTop: $save_message.offset().top - 100
             });

         } );
        e.preventDefault();
    });


    //Календарь на input

    $( document ).on( 'focus', '.datepicker_input', function(){

        $(this).datepicker({
            dateFormat: "dd.mm.yy",
            defaultDate: ''
        });
    });

    $('#order_form').on( 'input', 'input', function(){
        order_form.calc_fin_info();
    } );

    $('#order_form').on( 'change', 'select', function(){
        order_form.calc_fin_info();
    } );

    order_form.calc_fin_info();


    $( document ).on( 'focus', '.filter_country_autocomplete', function(){

        make_autocomplete( $(this), '/countries' );
    });

    $('.filter_reset').on( 'click', function(e){

        var form = $(this).parents('form');

        $( form ).find('input').each( function(){
            $(this).val('');
        } );

        $( form).find('select').each( function(){
            $(this).val(0);
        } );

        $( form ).submit();

    });

});


function make_autocomplete( el, data_path ) {

    var opt = {
        source : data_path,
        create: function() {
            $(this).data('ui-autocomplete')._renderItem = function( ul, item ) {
                return $( "<li>" )
                    .attr( "data-id", item.id )
                    .append( item.name )
                    .appendTo( ul );
            }
        },
        select: function( event, ui ) {
            event.preventDefault();
            $( event.target ).val( ui.item.name );
            $( event.target ).prev().val( ui.item.id );
        }
    };

    $( el ).autocomplete( opt );
}